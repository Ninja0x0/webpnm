from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login', views.loginCheck, name='login'),
    url(r'^signup', views.signup, name='signup'),
    url(r'^submit', views.submitCheck, name='submit'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^dashboard/',views.dashboard,name="dashboard"),
    url(r'^dashboard',views.dashboard,name="dashboard"),
    url(r'^settings/', views.settings)
]