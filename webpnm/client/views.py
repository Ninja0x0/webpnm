from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from api import  dbconnect
import json
import os
import api.config as config


NETWORK_NAME =None
client= dbconnect.DbConnect()
#

# Create your views here.
work_dir = os.getcwd()


def loginCheck(request):
    return render(request,"login.html")



def submitCheck(request):
    json_data=json.loads(request.body.decode('utf-8'))
    mongodb= dbconnect.DbConnect()
    mongodb.connect_db("openpnm")
    if json_data["type"]=="login":
        username=request.GET["username"];
        password=request.GET["password"];
        result=mongodb.loginUser(username,password)
        print(result)
        if(result["message"]=="ok"):
            request.session["username"]=username
            request.session["password"]=password

    elif json_data["type"]=="create":

        username=request.GET["username"];
        email=request.GET["email"];
        password=request.GET["password"];
        result=mongodb.findUser(username,email,password)
        if(result["message"]=="ok"):
            request.session["username"]=username
            request.session["password"]=password
            request.session["email"]=email
    if "username" in request.session:
        mongodb.setSettings(getCredentials(request), config.GUI_SETTINGS_DEFAULT)
    return JsonResponse(result,safe=False)

def checkSession(request):
    if request.session.get("username",False):
        mongodb= dbconnect.DbConnect()
        mongodb.connect_db("openpnm")
        result=mongodb.getUserData(request.session["username"],request.session["password"])
        print(result)
        result=json.loads(result)["networklist"]
        data={"creds":{"username":request.session["username"],"password":request.session["password"]},
              "lists":result}
        return JsonResponse(data,safe=False)
    else:
         return JsonResponse({"message":"redirect"},safe=False)
def logout(request):
    try:
        del request.session['username']
        del request.session["password"]
        del request.session["email"]
    except KeyError:
        pass
    return JsonResponse({"message":"ok","code":"100"},safe=False)


def signup(request):
    return render(request,"signup.html")

def svg(request,name):
    svg_path=os.path.join(work_dir,"public","svg",name+".svg")
    image_data=open(svg_path).read()
    return HttpResponse(image_data,content_type="image/svg")

def getCredentials(request):
    global NETWORK_NAME
    username = request.session["username"]
    password = request.session["password"]
    return {"username": username, "password": password,"network_name":NETWORK_NAME}

def dashboard(request):
    client.connect_db("openpnm")
    credentials=getCredentials(request);
    json_data=json.loads(client.getSettings(credentials))
    if json_data is None:
        data={}
    else:
        data=json_data["data"]

    return render(request,"dashboard.html",data)

def settings(request):
    client.connect_db("openpnm")
    credentials=getCredentials(request)
    print(request.GET['settings'])
    if(request.GET['settings'] == "true"):
        new_settings = json.loads(request.body.decode('utf-8'))
        new_settings.update(credentials)
        client.setSettings(credentials,new_settings)
        return HttpResponseRedirect("".join(["http://",request.get_host(),"/web/dashboard"]))
    else:
        settings_data=client.getSettings(credentials)
        return  JsonResponse(json.loads(settings_data),safe=False)

