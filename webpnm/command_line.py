import os
import subprocess
from sys import platform
import inspect
import webpnm
def main():
    path = os.path.dirname(os.path.abspath(__file__))
    if os.environ.get('MONGODB_PATH', None) is None:
        print("SET MONGODB PATH")
        os.environ['MONGODB_PATH'] = input("Enter Mongodb path:")

    host  =  input("host[localhost]:") or "127.0.0.1"
    port = input("Port[8000]:") or "8000"
    mongo_path =os.path.join(os.environ['MONGODB_PATH'],"mongod.exe")
    path_db = os.path.join(path,"db")
    manage_file = os.path.join(path,"manage.py")
    print()
    print(path_db)
    if platform == "linux" or platform =="linux2":
        rc = subprocess.call(["which","mongod"])
        mongo_path = "".join(['start "" ','"',str(mongo_path),'"'," --dbpath ",path_db," --port 27018"])
    elif platform == "win32":
        rc = subprocess.call(["where", "mongod"])
        mongo_path = "".join(['start "" ','"', str(mongo_path), '"'," --dbpath ",path_db," --port 27018"])

    try:
        os.system(mongo_path)
    except subprocess.CalledProcessError as e:
        print(e)
    server = " ".join(["python", manage_file, "migrate","&","python",manage_file,"runserver",host+":"+port])

    os.system(server)

