import importlib
import inspect
import re

OPEN_PNM="openpnm"
OPEN_PNM_GEOMETRY="openpnm.models.geometry"
OPEN_PNM_PHASES="openpnm.models.phases"
OPEN_PNM_PHYSICS="openpnm.models.physics"

class ModulesFetch:
    def  __init__(self):
        super(ModulesFetch, self).__init__()

    def getModelsData(self,mod_name):
        data={}
        path=OPEN_PNM+".models"+"."+mod_name;
        mod=importlib.import_module(path)
        modules=inspect.getmembers(mod,predicate=inspect.ismodule)
        for name,mod_handle in modules:
            mod=importlib.import_module(path+"."+name)
            data[name]={}
            functions=inspect.getmembers(mod,predicate=inspect.isfunction)
            for func_name,func_handle in functions:
                data[name][func_name]={}
                sig=inspect.signature(func_handle);
                parameters=sig.parameters.items()
                for p_name,p_value in parameters:
                    if p_value not in ("geometry","phase","network"):
                        arr=str(p_value).split("=")
                        if len(arr) == 1:
                            if arr[0] not in ("self","**kwargs","*args"):
                                data[name][func_name][p_name]=""
                        elif len(arr) == 2:
                            arr[1]=arr[1].replace("'","")
                            data[name][func_name][p_name]=arr[1]
        return data

    def attachGeoModels(self,geometry,model_json):
        for model_name in model_json:
            path=OPEN_PNM_GEOMETRY+model_name;
            mod=importlib.import_module(path);
            for method_name in model_json[model_name]:
                parameters=model_json[model_name][method_name];
                method=getattr(mod,method_name)
                propname=model_name.replace("_",".");
                parameters=self.checkValue(parameters)
                geometry.models.add(propname=propname,model=method,**parameters)
                return geometry

    def attachPhaseModels(self,phase,model_json):
        for model_name in model_json:
            path=OPEN_PNM_PHASES+model_name;
            mod=importlib.import_module(path);
            for method_name in model_json[model_name]:
                parameters=model_json[model_name][method_name];
                method=getattr(mod,method_name)
                propname=model_name.replace("_",".");
                parameters=self.checkValue(parameters)
                phase.models.add(propname=propname,model=method,**parameters)
                return phase

    def checkValue(self,parameters):
        for key in parameters:
            if re.match("^\d+?\.\d+?$", parameters[key]) is None:
                parameters[key]
            else:
                parameters[key]=float(parameters[key])

        return parameters
