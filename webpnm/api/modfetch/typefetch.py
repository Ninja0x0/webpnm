
import importlib
import inspect
import types

STRING_ALOG="openpnm.algorithm"

class TypeFetch:
    def __init__(self):
        super(TypeFetch, self).__init__()

    def getClasses(self,type_name):
        data={}

        path="openpnm."+type_name
        mod=importlib.import_module(path)
        classlist=inspect.getmembers(mod,predicate=inspect.isclass)
        for class_values in classlist:
            (class_name,class_handle)=class_values
            data[class_name]={}
            funclist=inspect.getmembers(class_handle,predicate=inspect.isfunction)
            for func_values in funclist :
                (func_name,func_handle)=func_values
                if func_name == "__init__":
                    sig=inspect.signature(func_handle)
                    for key,value in sig.parameters.items():
                        arr=str(value).split("=")
                        if len(arr) == 1:
                            if arr[0] not in ("self","**kwargs","*args"):
                                data[class_name][arr[0]]=""
                        elif len(arr) == 2:
                            arr[1]=arr[1].replace("'","")
                            data[class_name][arr[0]]=arr[1]
                    break
        return data

## fetch all methods of classess
    def getMethods(self,type):
        data = {}
        path = "openpnm."+type;
        mod = importlib.import_module(path)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            data[class_name] = {}
            funclist = inspect.getmembers(class_handle, predicate=inspect.isfunction)
            newlist = []
            for values in funclist:
                if isinstance(values[1], types.FunctionType):
                    if values[1].__module__ == class_handle.__module__:
                        newlist.append(values)
            for func_values in newlist:
                (func_name, func_handle) = func_values
                data[class_name][func_name] = {}
                if func_name != "__init__":
                    data[class_name][func_name] = {}
                sig = inspect.signature(func_handle)
                for key, value in sig.parameters.items():
                    arr = str(value).split("=")
                    if len(arr) == 1:
                        if arr[0] not in ("self", "**kwargs", "*args"):
                            data[class_name][func_name][arr[0]] = ""
                    elif len(arr) == 2:
                        arr[1] = arr[1].replace("'", "")
                        data[class_name][func_name][arr[0]] = arr[1]
        return data


    def getAlgoMethods(self):
        data={}
        mod = importlib.import_module(STRING_ALOG)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            data[class_name] = {}
            funclist = inspect.getmembers(class_handle, predicate=inspect.ismethod)
