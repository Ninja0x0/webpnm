import importlib
import inspect
import json
import os
import random
import urllib

import h5py as hdf5
import numpy as np
import openpnm
import requests
import scipy as sp
import scipy.sparse as sprs
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.utils.encoding import smart_str

from openpnm import topotools
from openpnm import  algorithms as pnm_alogs
from openpnm.io import HDF5
from openpnm.io import XDMF

import api as appApi
import api.config as config
from api import modfetch, dbconnect
from api.modfetch.typefetch import TypeFetch

ws = openpnm.Workspace()
CLIENT_ID = "a54aa442db2ed60db7b8"
CLIENT_SECRET = "7e374be428e8e5182239f896ca1e3a78bc360778"
client = dbconnect.DbConnect()

OPEN_PNM_NETWORK = "openpnm.network"
OPEN_PNM_GEOMETRY = "openpnm.geometry"
OPEN_PNM_PHASES = "openpnm.phases"
OPEN_PNM_PHYSICS = "openpnm.physics"


# Create your views here.
##below method fetch pores and throats labels
# handling responses
# try:
#     someFunction()
# except Exception as ex:
#     template = "An exception of type {0} occurred. Arguments:\n{1!r}"
#     message = template.format(type(ex).__name__, ex.args)
#     print message




def getCredentials(request):
    username = request.session["username"]
    password = request.session["password"]
    return {"username": username, "password": password}


def boolarray(value):
    for a in value:
        if a == False:
            return False;
    return True;


################################# Saving and deleting Network from database #################
## to dos
## 1.)checking whether session name and passwrod similar to send along with url (security)
## 2.)Instead of returning password return randomly generated session id (remove password field in next iteration) (security)
def saveProject(request, name, labels_data=None):
    client.connect_db("openpnm")

    json_data = json.loads(request.body.decode('utf-8'))
    credentials = getCredentials(request)
    credentials["network_name"] = json_data["name"]
    credentials["project_name"] = name

    if labels_data is not None:
        json_data["geo_data"] = labels_data


    client.saveProjectInDb(credentials=credentials, network_json=json_data)
    return JsonResponse({"operation": "success", "message": "network saved"}, safe=False)



def saveDropProject(request,labels_data,project):
    client.connect_db("openpnm")
    json_data = {}
    network_name=createNetworkJson(project,json_data)
    createGeometryJson(project,json_data)
    createPhasesJson(project,json_data)
    createPhysicsJson(project,json_data)

    credentials = getCredentials(request)
    credentials["network_name"] = network_name
    credentials["project_name"] = project.name

    if labels_data is not None:
        json_data["geo_data"] = labels_data


    client.saveProjectInDb(credentials=credentials, network_json=json_data)
    return JsonResponse({"operation": "success", "message": "network saved"}, safe=False)

####### methods for creating json from pnm file #######
def createNetworkJson(project,json_data):
    network=project.network
    json_data["name"]=network.name
    json_data["type"]=network.__module__.split(".")[2]
    json_data["inputs"]={}
    return network.name

def createGeometryJson(project,json_data):
    geometry=json_data["geometries"]={}
    network=project.network
    for k,v in project.geometries().items():
        geometry[k]={}
        geometry[k]["name"]=v.name
        geometry[k]["type"] = v.__module__.split(".")[2]
        for s in network.labels():
            if "pore" in s:
                pore_label="_".join(s.split("."))
            elif "throat" in s:
                throat_label="_".join(s.split("."))

        geometry[k]["pores"]=pore_label
        geometry[k]["throats"]=throat_label
        geometry[k]["models"] = {}
        geometry[k]["built_models"] = {}
        geometry[k]["built_change"] = {}
    return

def createPhasesJson(project,json_data):
    phases=json_data["phases"]={}
    for k,v in project.phases().items():
        phases[k]={}
        phases[k]["name"]=v.name
        phases[k]["type"]=v.__module__.split(".")[2]
        phases[k]["models"] = {}
        phases[k]["built_models"] ={}
        phases[k]["built_change"] = {}

    return

def createPhysicsJson(project,json_data):
    physics=json_data["physics"]={}
    for k,v in project.physics().items():
        physics[k]={}
        physics[k]["name"]=v.name
        physics[k]["geometry"]=project.find_geometry(project._get_object_by_name(k)).name
        physics[k]["phase"]=project.find_phase(project._get_object_by_name(k)).name
        physics[k]["type"]=v.__module__.split(".")[2]
        physics[k]["models"]={}
        physics[k]["built_models"] = {}
        physics[k]["built_change"] = {}

    return
### incomplete to add attachded models ###
def createModelsJson(project,type,json_data):
    json_data["built_change"]={}
    json_data["built_models"]={}
    json_data["models"]={}


    return
#############################################################
def loadProject(request):
    if request.session.get("username", False):
        client.connect_db("openpnm")
        credentials = getCredentials(request)
        json_string = client.loadProjectFromDb(credentials=credentials)
        json_data = json.loads(json_string)
        response = JsonResponse(json_data, safe=False)
        response["username"] = request.session["username"]
        response["password"] = request.session["password"]
        return response
    else:
        return JsonResponse({"message": "redirect", "path": "/web/login"}, safe=False)


def removeProject(request, name):
    client.connect_db("openpnm")
    credentials = getCredentials(request)
    credentials["project_name"] = name
    ## remove from database
    client.removeProjectFromDb(credentials=credentials)
    ## remove pnm and h5 file
    appApi.removeNetwork(request, name)

    return JsonResponse({"operation": "success", "message": "network removed"}, safe=False)


############################################################################################



###############################################################
def GeometryHealth(request, project_name, pore_label, throat_label):
    json_check = None
    data_array = np.array([])
    json_data = json.loads(request.body.decode('utf-8'))
    network_name = json_data["name"]
    geometries = json_data["geometries"]
    credentials = getCredentials(request)
    credentials["network_name"] = project_name
    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    h5 = hdf5.File(dir_path, 'r')
    pore_data = np.array(h5[project_name][network_name][pore_label])
    throat_data = np.array(h5[project_name][network_name][throat_label])
    for geo in geometries:
        geo_data = np.array(h5[project_name][network_name][geometries[geo]["pores"]])
        data_array = np.append(data_array, np.intersect1d(np.where(pore_data),
                                                          np.where(geo_data)))
    if data_array.size == 0:
        json_check = True
    else:
        json_check = False
        title = "Pores Collision"
        message = "**** any message ***"

    json_response = {"present": json_check}
    h5.close()
    return JsonResponse(json_response, safe=False)


def createLabels(request, name):
    request.session["username"] = "nivan"
    json_data = json.loads(request.body.decode('utf-8'))
    network = appApi.NetworkStorage(request, name)
    coords = network['pore.coords']
    for names in json_data:
        result = []
        values = json_data[names]["values"]
        boolvalues = coords < values
        result = [boolarray(a) for a in boolvalues]
        network["pore." + names] = np.array(result)
    appApi.NetworkStorage(request, name, network=network)
    return JsonResponse(json_data, safe=True)


###########--below methods are used to create label using logic in database ############
def createLogicLabels(request, project_name):
    json_data = json.loads(request.body.decode('utf-8'))
    client.connect_db('openpnm')
    new_json = {}
    credentials = getCredentials(request)
    credentials["network_name"] = json_data["network_name"]
    credentials["project_name"] = project_name
    label_name = json_data["label_name"]
    label_type = json_data["label_type"]
    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    h5 = hdf5.File(dir_path, 'r')

    createLabelData(json_data[label_name], new_json)
    label_data = parse(data=new_json, handle=h5, credentials=credentials, val=np.array([]))
    h5.close()

    if label_type == "pores":
        label_name = "pore_" + label_name
    else:
        label_name = "throat_" + label_name
    setH5Data(dir_path, credentials, label_name, label_data)
    client.updateGeoData(credentials=credentials, label_type=label_type, label_name=label_name)
    h5.close()

    response = {}
    response["type"] = label_type
    response["label_name"] = label_name
    response["network_name"] = credentials["network_name"]
    response["project_name"] = credentials["project_name"]

    return JsonResponse(response, safe=True)



def createPoreLabels(request, project_name, label_name,type):
    client.connect_db('openpnm')
    json_data = json.loads(request.body.decode('utf-8'))
    credentials = getCredentials(request)
    credentials["network_name"] = json_data["network_name"]
    credentials["project_name"] = project_name
    credentials["label_name"] = label_name

    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    h5 = hdf5.File(dir_path, 'r')
    pore_array = parsePoreLogic(handle=h5, credentials=credentials, json_data=json_data,type=type)
    h5.close()
    setH5Data(dir_path, credentials, "_".join(["pore", label_name]), np.array(pore_array))
    client.updateGeoData(credentials=credentials, label_type="pores", label_name="pore_" + label_name)
    response = {}
    response["type"] = "pores"
    response["label_name"] = "pore_" + label_name
    response["network_name"] = credentials["network_name"]
    response["project_name"] = credentials["project_name"]
    return JsonResponse(response, safe=True)

def createThroatLabels(request, project_name, label_name):
    client.connect_db('openpnm')
    json_data = json.loads(request.body.decode('utf-8'))
    credentials = getCredentials(request)
    credentials["network_name"] = json_data["network_name"]
    credentials["project_name"] = project_name
    credentials["label_name"] = label_name

    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    h5 = hdf5.File(dir_path, 'r')
    throat_array = parseThroatLogic(handle=h5, credentials=credentials, json_data=json_data)
    h5.close()
    setH5Data(dir_path, credentials, "_".join(["throat", label_name]), np.array(throat_array))
    client.updateGeoData(credentials=credentials, label_type="throats", label_name="throat_" + label_name)

    response = {}
    response["type"] = "throats"
    response["label_name"] = "throat_" + label_name
    response["network_name"] = credentials["network_name"]
    response["project_name"] = credentials["project_name"]

    return JsonResponse(response, safe=True)

def parsePoreLogic(handle, credentials, json_data,type):
    a=None
    conns = getH5Data(handle, credentials, "throat_conns")
    data = sp.arange(conns.shape[0]) + 1
    N = sp.amax(conns) + 1
    am = sprs.coo_matrix((data, conns.T), shape=(N, N))
    am -= am.T
    am.data -= 1
    am = am.tolil()

    for key, value in json_data[credentials["label_name"]].items():
        for k, v in value.items():
            sites_bonds = getH5Data(handle, credentials, k)
            sites_bonds = np.where(sites_bonds == True)[0]
            if "neighbor" in key:
                if type == "pores":
                    a = topotools.find_neighbor_sites(sites_bonds, am, logic="union")
                else:
                    a = topotools.find_connected_sites(sites_bonds,am,logic="union")
            elif "common" in key:
                if type == "pores":
                    a = topotools.find_neighbor_sites(sites_bonds, am, logic="intersection")
                else:
                    a = topotools.find_connected_sites(sites_bonds, am, logic="exclusive_or")
            elif "unique" in key:
                if type == "pores":
                    a = topotools.find_neighbor_sites(sites_bonds, am, logic="exclusive_or")
                else:
                    a = topotools.find_connected_sites(sites_bonds,am,logic="exclusive_or")

    new_pore = getH5Data(handle, credentials, "pore_none")
    new_pore[a] = True
    return new_pore

def parseThroatLogic(handle, credentials, json_data):
    a=None
    conns = getH5Data(handle, credentials, "throat_conns")
    data = sp.arange(conns.shape[0]) + 1
    N = sp.amax(conns) + 1
    am = sprs.coo_matrix((data, conns.T), shape=(N, N))
    am -= am.T
    am.data -= 1
    am = am.tolil()
    for key, value in json_data[credentials["label_name"]].items():
        for k, v in value.items():
            sites = getH5Data(handle, credentials, k)
            sites = np.where(sites == True)[0]
            if "union" in key:
                a = topotools.find_neighbor_bonds(sites, am, logic="union")
            elif "intersection" in key:
                a = topotools.find_neighbor_bonds(sites, am, logic="intersection")
            elif "exclusive_or" in key:
                a = topotools.find_neighbor_bonds(sites, am, logic="exclusive_or")

    new_throat = getH5Data(handle, credentials, "throat_none")
    new_throat[a] = True
    return new_throat


## refractor below method code ***
## break points of this method
## need validation to check every logic has two input else return error
##need validation to check label name doesnot have specific key words like union intersection which are already reserved
##need validation to check every pore and throat labels start with pore. and throat.
##improvment of this code to parse according to the type rather the name in json like instead union which has type logic we parse checking logic type
##

def parse(data, handle, credentials, val):
    for k, v in data.items():
        if type(v) is list:
            arr = []
            for index in v:
                if type(index) is not dict:
                    arr.append(index)
                else:
                    val = parse(data=index, handle=handle, credentials=credentials, val=val)

            if k in "union":
                val = getH5Data(handle=handle, credentials=credentials, label_name="pore_none")
                for index in arr:
                    val = val + getH5Data(handle=handle, credentials=credentials, label_name=index)
            if k in "intersection":
                val = getH5Data(handle=handle, credentials=credentials, label_name="pore_all")
                for index in arr:
                    val = val * getH5Data(handle=handle, credentials=credentials, label_name=index)
            return val
    return val


def getH5Data(handle, credentials, label_name):
    symlink = "/".join([credentials["project_name"], credentials["network_name"], label_name])
    return np.array(handle[symlink])


def setH5Data(dir_path, credentials, label_name, data):
    handle = hdf5.File(dir_path, 'a')
    symlink = "/".join([credentials["project_name"], credentials["network_name"]])
    ref = handle[symlink]
    if label_name in list(ref.keys()):
        val = ref[label_name]
        val[...] = data
    else:
        ref[label_name] = data
    handle.close();


def createLabelData(json_data, new_data={}):
    for key, value in json_data.items():
        if type(value) is dict and bool(value):
            for k, v in value.items():
                if bool(v) == False:
                    new_data[key] = []
                    break;
                else:
                    new_data[key] = {}

            if type(new_data[key]) is list:
                for k, v in value.items():
                    if type(v) is dict and bool(v):
                        data = {k: v}
                        new_data[key].append(data)
                        createLabelData(data, new_data[key][-1])
                    else:
                        new_data[key].append(k)
    return new_data


def mapDBNames(name):
    if name == "union":
        return "$setUnion"
    elif name == "intersection":
        return "$setIntersection"
    elif name == "difference":
        return "$setDifference"
    elif "pore." in name:
        return "$" + "_".join(name.split("."))
    elif "throat." in name:
        return "$" + "_".join(name.split("."))
    return name


##---------------------------------------------------------------------------------##



def createDbLabels(request, name):
    json_data = json.loads(request.body.decode('utf-8'))
    db = dbconnect.DbConnect()
    db.connect_db('openpnm')
    collection = db.getCoords(credentials=getCredentials(request), network_name=name)
    coords = np.array(collection[0]['pore_coords'])
    for key_name in json_data:
        result = []
        values = json_data[key_name]["values"]
        boolvalues = coords < values
        result = [boolarray(a) for a in boolvalues]
        label_indices, = np.where(result)
        db.addUserLabels(getCredentials(request), name, key_name, label_indices, )
        return JsonResponse(json_data, safe=True)


def changeLocation(request):
    request.session["username"] = "nivan"



def createLabelConditon(request,project_name,network_name,label_name):
    client.connect_db('openpnm')
    json_data = json.loads(request.body.decode('utf-8'))
    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    handle = hdf5.File(dir_path, 'r')
    credentials = getCredentials(request)
    credentials["project_name"] = project_name
    credentials["network_name"] = network_name
    pore_coords = getH5Data(handle,credentials,"pore_coords")
    first_key = list(json_data)[0]
    mask = json_data[first_key]["mask"]
    temp_condition = eval('pore_coords[:,int(json_data[first_key]["direction"])]'+json_data[first_key]["operation"]+json_data[first_key]["coords"])
    print(temp_condition)
    for key in list(json_data)[1:]:
        data = json_data[key]
        next_condition = eval('pore_coords[:,int(data["direction"])]'+data["operation"]+data["coords"])
        temp_condition = eval('(temp_condition)'+mask+'(next_condition)')
        mask = data["mask"]
    handle.close()
    setH5Data(dir_path,credentials,label_name,temp_condition)
    client.updateGeoData(credentials=credentials, label_type="pores", label_name=label_name)
    response={}
    response["type"] = "pores"
    response["label_name"] = label_name
    response["project_name"] = project_name
    response["network_name"] = network_name
    return JsonResponse(response,safe=True)


##method returns labels of pores and throats
##change method name according to function
##using locks mutex
def project(request, name,workspace=None):
    if workspace is None:
        json_data = json.loads(request.body.decode("utf-8"))
        network_name = json_data["name"]
        path = OPEN_PNM_NETWORK
        mod = importlib.import_module(path)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            if class_name == json_data["type"]:
                parameters = appApi.jsonCheck(json_data["inputs"])
                parameters["name"] = network_name
                network = class_handle(**parameters)
                network.project.name = name
                network['pore.none'] = network['pore.all'] != True
                network['throat.none'] = network['throat.all'] != True
    else:
        network=workspace[name].network





    ##fetching pore and throat labels from network
    labels_data = {"pores": [], "throats": []}
    for key in network.labels():
        key="_".join(key.split("."))
        if "pore" not in key:
            if key not in labels_data["throats"]:
                labels_data["throats"].append(key)
        else:
            if key not in labels_data["pores"]:
                labels_data["pores"].append("_".join(key.split(".")))
    labels_data["attached_pores"] = {}
    labels_data["attached_throats"] = {}
    ## save network to mongo
    if workspace is None:
        saveProject(request=request, name=name, labels_data=labels_data)

        h5 = HDF5().to_hdf5(network=network,
                            filename=appApi.storageFS(request, 'Networks') + "/" + name, categorize_by=["project"])
        h5.close()
    else:
        saveDropProject(request,labels_data,workspace[name])
        clearProject(workspace[name])
        h5 = HDF5().to_hdf5(network=network,
                            filename=appApi.storageFS(request, 'Networks') + "/" + name, categorize_by=["project"]).close()

          ## delete all geometeries ,phases and physics attached to network object

    ## save into pnm format
    appApi.NetworkStorage(request, name, network=network, object_list=[network])


    labels_data["network_name"] = network.name
    labels_data["project_name"] = name
    ws.clear()
    return JsonResponse(labels_data, safe=True)



def clearProject(project):
    for k,v in project.geometries().items():
        project.remove(v)
    for k,v in project.phases().items():
        project.remove(v)
    for k,v in project.physics().items():
        project.remove(v)

def geometryEx(request):
    db = dbconnect.DbConnect()
    db.connect_db("openpnm")

    json_data = json.loads(request.body.decode('utf-8'))
    network_name = json_data["network_name"]
    geometry_data = json_data["data"]

    ## purpose of the code unknown
    # result=db.addGeometry(getCredentials(request),json_data)
    # if result == False:
    #     return JsonResponse(status=404, data={'status': 'false', 'message': "Geometry already present"})

    network = openpnm.network.Cubic(shape=[3, 3, 3])
    path = OPEN_PNM_GEOMETRY
    mod = importlib.import_module(path)
    classlist = inspect.getmembers(mod, predicate=inspect.isclass)
    for class_values in classlist:
        (class_name, class_handle) = class_values
        if class_name == geometry_data["type"]:
            parameters = appApi.jsonCheck(geometry_data["inputs"])
            parameters["network"] = network
            parameters["pores"] = network.pores('all')
            parameters["throats"] = network.throats('all')
            geometry = class_handle(**parameters)
    data = {"models": {}}
    for models_name in geometry.models:
        data["models"][models_name] = {}

    return JsonResponse(data, safe=False)


def phases(request):
    phase = ''
    json_data = json.loads(request.body.decode('utf-8'))
    # network_name=json_data["network_name"]
    network = openpnm.network.Cubic(shape=[3, 3, 3])
    phase_data = json_data["data"]
    path = OPEN_PNM_PHASES
    mod = importlib.import_module(path)
    classlist = inspect.getmembers(mod, predicate=inspect.isclass)
    for class_values in classlist:
        (class_name, class_handle) = class_values
        if class_name == phase_data["type"]:
            parameters = appApi.jsonCheck(phase_data["inputs"])
            parameters["name"] = phase_data["name"]
            parameters["network"] = network
            phase = class_handle(**parameters)

    data = {"models": {}}
    for models_name in phase.models:
        data["models"][models_name] = {}

    return JsonResponse(data, safe=False)


##problem of testnet
def physics(request):
    json_data = json.loads(request.body.decode('utf-8'))
    # network_name=json_data["network_name"]
    network = openpnm.network.Cubic(shape=[3, 3, 3])
    physics_data = json_data["data"]
    path = OPEN_PNM_PHYSICS
    mod = importlib.import_module(path)
    classlist = inspect.getmembers(mod, predicate=inspect.isclass)
    # print(classlist)
    # for class_values in classlist:
    #     (class_name,class_handle)=class_values
    #     if class_name == physics_data["type"]:
    #         parameters=appApi.jsonCheck(physics_data["inputs"])
    #         parameters["name"]=physics_data["name"]
    #         parameters["network"]=network
    #         physics=class_handle(**parameters)
    data = {"models": {}}
    physics.models = {}
    for models_name in physics.models:
        data["models"][models_name] = {}

    return JsonResponse(data, safe=False)


# def attachedModels(request,type,type_name):
#     net=openpnm.network.Cubic([10,10,10])
#     type_handle=importlib.import_module("openpnm."+type)
#     print(type_handle)
#     obj=getattr(type_handle,type_name)(network=net)
#     print(obj)
#     data = map(lambda x: "_".join(x.split(".")),list(obj.models))
#     return JsonResponse(list(data),safe=False)
def attachedModels(request, type, type_name):
    type_handle = importlib.import_module("openpnm." + type)
    obj = getattr(type_handle, type_name)(settings={'freeze_models': True})
    json_data={}
    for k,v in obj.models.items():
        k="_".join(k.split("."))
        data=str(v["model"].__module__).split(".")
        name = v["model"].__name__
        json_data.update({
            k: {data[-1]: {name: {}}}
        })
        for el,val in v.items():
            if el is not "model" and el is not "regen_mode":
                json_data[k][data[-1]][name][el]=str(val)


    return JsonResponse(json_data, safe=False)


def getModelsData(request, type):
    mod_obj = modfetch.ModulesFetch()
    json_data = mod_obj.getModelsData(type)
    return JsonResponse(json_data, safe=True)


def helpDocumentation(request):
    mod = request.GET["module"]
    name = request.GET["class"]
    mod = "openpnm." + mod
    mod = importlib.import_module(mod)
    classlist = inspect.getmembers(mod, predicate=inspect.isclass)
    for class_value in classlist:
        (class_name, class_handle) = class_value
        if class_name == name:
            return HttpResponse(class_handle.__doc__, content_type="text/plain")

    return HttpResponse("No Documentation Provided", content_type="text/plain")


def github(request):
    url = "https://github.com/login/oauth/authorize"
    values = {"client_id": CLIENT_ID}
    params = urllib.parse.urlencode(values)
    url = url + "?%s" % params
    return HttpResponseRedirect(url)


def authorize(request):
    url = "https://github.com/login/oauth/access_token"
    access_url = "https://api.github.com/user?access_token="
    code = request.GET["code"]
    params = {"client_id": CLIENT_ID, "client_secret": CLIENT_SECRET, "code": code}
    headers = {"Accept": "application/json"}
    values = requests.post(url, params=params, headers=headers)
    json_data = json.loads(values.content)
    url_data = requests.get(access_url + json_data["access_token"])
    json_data = json.loads(url_data.content)
    username = str(json_data["login"])
    password = str(json_data['id'])
    mongodb = dbconnect.DbConnect()
    mongodb.connect_db("openpnm")
    result = mongodb.loginUser(username, password)


    if result["message"] == "ok":
        request.session["username"] = username
        request.session["password"] = password

        return HttpResponseRedirect("http://"+request.get_host()+"/web/dashboard")
    else:
        mongodb.createUser(username=username, email=username, password=password)
        request.session["username"] = username
        request.session["password"] = password
        mongodb.setSettings(getCredentials(request), config.GUI_SETTINGS_DEFAULT)
        return HttpResponseRedirect("http://"+request.get_host()+"/web/dashboard")


## Failures (to-do)
## checking method paramters doesnt hae null values
##_----------------------------------------------------------------------------------------------------------------------------------___##

#########################################################################################################################################

def applyGeometry(network, geometires,object_list=[]):
    geo_objects = {}
    for geometry_data in geometires:
        # network_name=json_data["network_name"]
        geometry_data = geometires[geometry_data]
        # network=appApi.NetworkStorage(request,network_name)
        path = OPEN_PNM_GEOMETRY
        mod = importlib.import_module(path)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            if class_name == geometry_data["type"]:
                parameters={}
                #parameters = appApi.jsonCheck(geometry_data["inputs"])
                parameters["name"] = geometry_data["name"]
                parameters["network"] = network
                parameters["pores"] = network.pores(".".join(geometry_data["pores"].split("_",1)))
                parameters['throats'] = network.throats(".".join(geometry_data["throats"].split("_",1)))
                # parameters["throats"]=network.find_neighbor_throats(network.pores(geometry_data["pores"]),mode=mode)
                network.pop("pore." + geometry_data["name"], "None")
                network.pop("throat." + geometry_data["name"], "None")
                geo = class_handle(**parameters)
                object_list.append(geo)
                removeBuiltModels(geometry_data["built_change"], geo)
                applyModels("openpnm.models.geometry", geometry_data["built_change"], geo)
                applyModels("openpnm.models.geometry", geometry_data["models"], geo)
                # geo_objects[geometry_data["name"]]=geo.name

    return network


def applyPhase(network, phases,object_list=[]):
    phase_objects = {}
    for phase_data in phases:
        phase_data = phases[phase_data]
        path = OPEN_PNM_PHASES
        mod = importlib.import_module(path)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            if class_name == phase_data["type"]:
                #parameters = appApi.jsonCheck(phase_data["inputs"])
                parameters={}
                parameters["name"] = phase_data["name"]
                parameters["network"] = network
                phase = class_handle(**parameters)
                object_list.append(phase)
                removeBuiltModels(phase_data["built_change"],phase)
                applyModels("openpnm.models.phase", phase_data["built_change"], phase)
                applyModels("openpnm.models.phase", phase_data["models"], phase)

                # phase_objects[phase_data["name"]]=phase.name
                # print(phase_objects)

    return network


def applyModels(model_mod, models_data, type_obj):
    for model_key, model_value in models_data.items():
        model_arr = model_key.split("_");
        propname = ".".join([model_arr[0], "_".join(model_arr[1:])])
        for key, value in model_value.items():
            if key == "misc":
                mod = importlib.import_module("openpnm.models.misc")
            else:
                mod = importlib.import_module(model_mod + "." + key)

            functions = inspect.getmembers(mod, predicate=inspect.isfunction)
            for method_name, method_inputs in value.items():
                for (func_name, func_handle) in functions:
                    if func_name == method_name:
                        method_value = method_inputs
                        parameters = appApi.jsonCheck(method_inputs)
                        if "target" in parameters:
                            del parameters["target"]

                        # method_value["regen_mode"] = 'normal'

                        type_obj.add_model(propname=propname, model=func_handle, regen_mode='normal', **parameters)

    return type_obj

def removeBuiltModels(built_models,type_obj):
    for key,value in built_models.items():
        key=".".join(key.split("_",1));
        if value in list(type_obj.models):
            del type_obj.models[key]



def applyPhysics(network, json_data,object_list=[]):
    path = OPEN_PNM_PHYSICS
    for physics_data in json_data:
        physics_data = json_data[physics_data]
        mod = importlib.import_module(path)
        classlist = inspect.getmembers(mod, predicate=inspect.isclass)
        for class_values in classlist:
            (class_name, class_handle) = class_values
            if class_name == physics_data["type"]:
                parameters = {}
                parameters["name"] = physics_data["name"]
                parameters["network"] = network
                geometry_name = physics_data["geometry"]
                geo_handle = network.project.geometries()[geometry_name]
                parameters["geometry"] = geo_handle
                phase_name = physics_data["phase"]
                phase_handle = network.project.phases()[phase_name]
                parameters["phase"] = phase_handle
                ##parameters["pores"]=geo.pores()
                ##parameters["throats"]=geo.throats()
                physics = class_handle(**parameters)
                object_list.append(physics)
                removeBuiltModels(physics_data["built_change"], physics)
                physics = applyModels("openpnm.models.physics", physics_data["built_change"], physics)
                physics = applyModels("openpnm.models.physics", physics_data["models"], physics)

    return network


def algorithms(network, algo_data,object_list=[]):
    path = "openpnm.algorithms"
    mod = importlib.import_module(path)
    classlist = inspect.getmembers(mod, predicate=inspect.isclass)
    for algo in algo_data:
        for class_values in classlist:
            (class_name, class_handle) = class_values
            if (class_name == algo):
                alg = class_handle(network=network)
                object_list.append(alg)
                for method_key in algo_data[algo]:
                    method_name = list(method_key.keys())[0]
                    method_handle = getattr(alg, method_name)
                    parameters = method_key[method_name]
                    for key, value in parameters.items():
                        ##--------------------------------------##
                        if key == "pores" or key == "throats":
                            parameters[key] = network.pores(value)
                        if key == "invading_phase" or key == "defending_phase" or key == "phase":
                            parameters[key] = network.project.phases()[value]
                            ##-------------------------------------##


                    method_handle(**parameters)
                    # alg.run()
    return network


def configurePoresAndThroats(path, project_name, network):
    network_name = network.name;
    h5 = hdf5.File(path, 'r')
    symlink = "/".join([project_name, network_name])
    ref = h5[symlink]
    for label_name in list(ref.keys()):
        pnm_label_name = ".".join(label_name.split("_"))
        network[pnm_label_name] = np.array(ref[label_name])

    h5.close()
    return network


def configureAndCreate(request, project_name, run=False):
    network_json = json.loads(request.body.decode('utf-8'))
    geometries = network_json["geometries"]
    phases = network_json["phases"]
    physics = network_json["physics"]
    object_list=[]
    #network = appApi.NetworkStorage(request, project_name)
    project_path = appApi.getProject(request,project_name)
    ws = openpnm.Workspace()
    ws.load_project(os.path.join(project_path,project_name))
    project = ws[project_name]
    network = project.network
    object_list.append(network)

    dir_path = appApi.storageFS(request, "Networks")
    dir_path = os.path.join(dir_path, project_name + ".hdf")
    clearProject(project)
    configurePoresAndThroats(dir_path, project_name, network)
    applyGeometry(network, geometries,object_list)
    applyPhase(network, phases,object_list)
    applyPhysics(network, physics,object_list)

    if run:
        simulation = network_json["simulation"]
        algorithms(network, algo_data=simulation,object_list=object_list)

    h5_path = os.path.join(appApi.storageFS(request, 'temp'), project_name)

    #del network["pore._id"]
    #del network["throat._id"]
    XDMF.save(network=network, filename=h5_path)

    return appApi.NetworkStorage(request, project_name, network,object_list)


def runAlgorithim(request, project_name):
    configureAndCreate(request, project_name, run=True)
    return JsonResponse({"message": "saved", "hash": generateHash(request)}, safe=False)


def getCode(module, type, parameters):
    mod = ".".join(["openpnm", module, type])
    if "project" in list(parameters):
        parameters["project"]="~"+parameters["project"]+"~"
    x=str(appApi.jsonCheck(parameters))
    x = x.replace("\'~", "")
    x = x.replace("~\'", "")
    param = "**" + x

    # param = param.replace("'", "")
    return mod + "(" + param + ")"


def getGeometryCode(network_name, data, script):
    for key, value in data.items():
        js_data = {}
        js_data["network"] = "~" + network_name + "~"
        js_data["name"] = value["name"]
        js_data["pores"] = "~" + network_name + ".pores('" + ".".join(value["pores"].split("_")) + "')~"
        js_data["throats"] = "~" + network_name + ".throats('" + ".".join(value["throats"].split("_")) + "')~"
        x = getCode("geometry", value["type"], js_data)
        x = x.replace("\'~", "")
        x = x.replace("~\'", "")
        x = x.replace("\"~", "")
        x = x.replace("~\"", "")
        script.append(value["name"] + "=" + x)
        getModelsCode(script, value["name"], "geometry", value["models"])
    return script


def getPhaseCode(network_name, data, script):
    for key, value in data.items():
        js_data = {}
        js_data["network"] = "~" + network_name + "~"
        js_data["name"] = value["name"]
        x = getCode("phases", value["type"], js_data)
        x = x.replace("\'~", "")
        x = x.replace("~\'", "")
        script.append(value["name"] + "=" + x)
        getModelsCode(script, value["name"], "phase", value["models"])
    return script


def getPhysicsCode(network_name, data, script):
    for key, value in data.items():
        js_data = {}
        js_data["network"] = "~" + network_name + "~"
        js_data["name"] = value["name"]
        js_data["geometry"] = "~" + value["geometry"] + "~"
        js_data["phase"] = "~" + value["phase"] + "~"
        x = getCode("physics", value["type"], js_data)

        x = x.replace("\'~", "")
        x = x.replace("~\'", "")
        script.append(value["name"] + "=" + x)
        getModelsCode(script, value["name"], "physics", value["models"])
    return script


def getModelsCode(script, obj, type, data):
    for model_key, model_value in data.items():
        js_data = {}
        model_name = ".".join(model_key.split("_"))
        mod = ".".join([obj, "models"])
        model_type = list(model_value.keys())[0]
        model_method = list(model_value[model_type].keys())[0]
        inputs = model_value[model_type][model_method]
        if "target" in list(inputs):
            del inputs["target"]
        js_data["model"] = "~" + ".".join(["openpnm.models", type, model_type, model_method]) + "~";
        js_data.update(inputs)
        js_data = appApi.jsonCheck(js_data)
        x = str(js_data)
        x = x.replace("\'~", "")
        x = x.replace("~\'", "")
        script.append(mod + "['" + model_name + "']=" + x)


def getLabelCode(network_name, data, script, type):
    temp = []
    ran = "lab_" + str(int(random.random() * 100))

    for value in list(data):
        if bool(data[value]):
            temp.append(getLabelCode(network_name, data[value], script, value))
        else:
            x=value.split("_")
            temp.append(x[1])
    if len(temp) > 1:
        script.append(ran + "=" + createLogicString(network_name, temp, type))
        return ran


def createLogicString(network_name, data, type):
    if type in "union" or type in "intersection":
        return network_name + ".pores(labels=" + str(data) + ", mode='" + type + "')"
    return ""

def createFinalLabel(network_name,script,label_name):
    val=script[-1].split("=",maxsplit=1)
    label_name=".".join(label_name.split("_"))
    script.append(network_name+"['"+label_name+"']=False")
    script.append(network_name + "['" + label_name + "']["+val[0]+"]=True")

def getAlgorithimCode(network_name, data, script):
    random_values = []
    for key, value in data.items():
        random_variable = "alg_" + str(int(random.random() * 100))
        js_data = {}
        js_data["network"] = "~" + network_name + "~"
        x = str(getCode("algorithms", key, js_data))
        x = x.replace("\'~", "")
        x = x.replace("~\'", "")
        script.append(random_variable + "=" + x)
        for obj in value:
            for method_name, parameters in obj.items():
                x = None;
                x = str(appApi.jsonCheck(getAlgoMethodCode(network_name, parameters)))
                x = x.replace("\"~", "")
                x = x.replace("~\"", "")
                x = x.replace("\'~", "")
                x = x.replace("~\'", "")
                m = method_name + "(**" + x + ")"
                script.append(random_variable + "." + m)
        script.append(random_variable + "." + "run()")


def getAlgoMethodCode(network_name, data):
    obj = data.copy()
    for k, v in data.items():
        if k in ["phase", "invading_phase", "defending_phase"]:
            obj[k] = "~" + v + "~"
        elif k in "pores":
            obj[k] = "~" + network_name + ".pores('" + ".".join(data["pores"].split("_")) + "')~"
        elif k in "throats":
            obj[k] = "~" + network_name + ".throats('" + ".".join(data["throats"].split("_")) + "')~"

    return obj

def importCode(data,script):
    for value in data:
        script.append("import "+value)
    return script

def downloadScript(request, project_name):
    download_path = appApi.storageFS(request, 'temp')
    json_data = json.loads(request.body.decode('utf-8'))
    data = json.loads(request.body)
    script_data = []
    importCode(["openpnm"],script_data)
    script_data.append("ws = openpnm.core.Workspace()")
    script_data.append(project_name+" = ws.new_project('"+project_name+"')")
    json_data["inputs"].update({'project':project_name,'name':json_data["name"]}) ## project and network name
    script_data.append(json_data["name"] + "=" + getCode("network", json_data["type"], json_data["inputs"]))
    if "label_collection" in list(json_data):
        for label in json_data["label_collection"]:
            for label_name,v in label.items():
                getLabelCode(json_data["name"],v[list(v)[0]],script_data,list(v)[0])
                createFinalLabel(json_data["name"],script_data,label_name)
    getGeometryCode(json_data["name"], json_data["geometries"], script_data)
    getPhaseCode(json_data["name"], json_data["phases"], script_data)
    getPhysicsCode(json_data["name"], json_data["physics"], script_data)

    if "simulation_collection" in list(json_data):
        for value in json_data["simulation_collection"]:
            getAlgorithimCode(json_data["name"], value, script_data)
    with open(os.path.join(download_path, project_name + ".py"), mode='wt', encoding='utf-8') as file:
        file.write('\n'.join(script_data))
    return JsonResponse({"message": "saved", "hash": generateHash(request), "project_name": project_name}, safe=False)


def getPythonFile(request, project_name, hash):
    download_path = appApi.storageFS(request, 'temp')
    file_path = os.path.join(download_path, project_name + ".py")
    if request.session["download_hash"] == str(hash):
        fp = open(file_path, "rb")
        response = HttpResponse(fp.read())
        response['Content-Type'] = 'application/octet-stream';  # mimetype is replaced by content_type for django 1.7
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(project_name + ".py")
        response['X-Sendfile'] = smart_str(file_path)
        return response


def getScript(request, project_name):
    data = json.loads(request.body.decode('utf-8'))
    script_data = []
    getCode("network", data["type"], data["inputs"])

    return JsonResponse({"message": "saved", "hash": generateHash(request)}, safe=False)


#############################################################################################################

def generate(request,project_name):
    download_path = appApi.storageFS(request, 'temp')
    configureAndCreate(request, project_name, run=False)
    
    return JsonResponse(
            {"message": "saved", "hash": generateHash(request),  "project_name": project_name},
            safe=False)


def generateHash(request):
    hash = random.getrandbits(128);
    request.session["download_hash"] = str(hash)
    return str(hash);


def getFile(request, project_name, type, hash_id):
    if request.session["download_hash"] == hash_id:
        if type == 'hdf':
            temp_dir = appApi.storageFS(request, 'temp')
            file_path = os.path.join(temp_dir, ".".join([project_name, "hdf"]))

        elif type == "xmf":
            temp_dir = appApi.storageFS(request, 'temp')
            file_path = os.path.join(temp_dir, ".".join([project_name, "xmf"]))
        else:
            temp_dir = appApi.storageFS(request, 'PNM')
            file_path = os.path.join(temp_dir, ".".join([project_name, type]))

        fp = open(file_path, "rb")
        response = HttpResponse(fp.read())
        response['Content-Type'] = 'application/octet-stream';  # mimetype is replaced by content_type for django 1.7
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(".".join([project_name, type]))
        response['X-Sendfile'] = smart_str(file_path)
        return response


def upload(request):
    if request.method == 'POST' and request.FILES['pnmfile']:
        myfile = request.FILES['pnmfile']
        project_name=request.POST["file-name"]
        project_name=project_name.replace(" ","")
        temp_dir = appApi.storageFS(request, 'PNM')
        with open(os.path.join(temp_dir,myfile.name), 'wb+') as destination:
            for chunk in myfile.chunks():
                destination.write(chunk)
        w=openpnm.Workspace()

        w.load_project(os.path.join(temp_dir,myfile.name))
        project(request, project_name,w)
        return JsonResponse({
            'uploaded_file_url': temp_dir
        },safe=False)






########################################################################################

def getTypesData(request, type):
    json_data = TypeFetch().getClasses(type.lower())
    print(json_data)
    return JsonResponse(json_data, safe=False)


def getMethodsData(request, type):
    data={}
    json_data = TypeFetch().getMethods(type.lower())
    # when using gui config.py file
    #
    #     parseAlgoJson(json_data)
    if type == "Algorithms":
        for key in list(json_data):
           data[key]=getattr(pnm_alogs, key)().settings['gui']
        json_data = data

        
    return JsonResponse(json_data, safe=False)


def parseAlgoJson(data):
    for key in list(data):
        for name in list(data[key]):
            if key not in list(config.ALGORITHM):
                del data[key]
            else:
                if name not in list(config.ALGORITHM[key]):
                    del data[key][name]
    ## this is  to append method of parent class
    for key in list(data):
        if config.PARENT[key] is not None:
            if "GenericTransport" in list(config.PARENT[key]):
                temp = dict(data[key])
                data[key].update(data["GenericTransport"])
                data[key].update(temp)
    return data
###################################################################################################################
