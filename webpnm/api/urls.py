from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^project/(?P<name>[-\w]+)$', views.project, name='project'), ## return pores and throats
    url(r'^geometry', views.geometryEx, name='geometry'), ## return models for specific type
    url(r'^phases', views.phases, name='phases'), ## return models
    url(r'^physics', views.physics, name='physics'), ## return models
    url(r'^types/(?P<type>[-\w]+)$', views.getTypesData, name='types'),
    url(r'^methods/(?P<type>[-\w]+)$', views.getMethodsData, name='methods'),
    url(r'^script/(?P<project_name>[-\w]+)$', views.downloadScript, name='script'),
    url(r'^python/(?P<project_name>[-\w]+)/(?P<hash>[-\w]+)$', views.getPythonFile, name='python'),
    url(r'^attach/(?P<type>[-\w]+)/(?P<type_name>[-\w]+)$', views.attachedModels, name='attach'),
    url(r'^models/(?P<type>[-\w]+)/$', views.getModelsData, name='models'),
    url(r'^run/(?P<project_name>[-\w]+)', views.runAlgorithim, name='run'),
    url(r'^file/(?P<project_name>[-\w]+)/(?P<type>[-\w]+)/(?P<hash_id>[-\w]+)$', views.getFile, name='download'), ##return pnm file
    url(r'^labels/(?P<name>[-\w]+)$', views.createDbLabels, name='labels'),
    url(r'^health/(?P<project_name>[-\w]+)/(?P<pore_label>[-\w]+)/(?P<throat_label>[-\w]+)$', views.GeometryHealth, name='healthEx'),
    url(r'^logiccondition/(?P<project_name>[-\w]+)/(?P<network_name>[-\w]+)/(?P<label_name>[-\w]+)$', views.createLabelConditon, name='label_condition'),
    url(r'^logiclabels/(?P<project_name>[-\w]+)$', views.createLogicLabels, name='logic_labels'),
    url(r'^throatlabels/(?P<project_name>[-\w]+)/(?P<label_name>[-\w]+)$', views.createThroatLabels, name='throat__labels'),
    url(r'^porelabels/(?P<project_name>[-\w]+)/(?P<label_name>[-\w]+)/(?P<type>[-\w]+)$', views.createPoreLabels, name='pore__labels'),
    url(r'^save/(?P<name>[-\w]+)$', views.saveProject, name='save_project'),
    url(r'^load', views.loadProject, name='load_project'),
    url(r'^remove/(?P<name>[-\w]+)$', views.removeProject, name='remove_project'),
    url(r'^generate/(?P<project_name>[-\w]+)$', views.generate, name='download_project'),
    url(r'^upload/$', views.upload, name='upload_pnm'),
    url(r'^help', views.helpDocumentation, name='help'),
    url(r'^github', views.github, name='github'),
    url(r'^authorize', views.authorize, name='authorize'),

]
