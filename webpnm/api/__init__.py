import pickle
import os
import openpnm

def storageFS(request,type):
    p_dir = os.getcwd()
    directory=os.path.join(p_dir,"Storage")
    if not os.path.exists(directory):
        os.makedirs(directory)
    user_dir=os.path.join(directory,request.session["username"])
    if not os.path.exists(user_dir):
        os.makedirs(user_dir)
    type_dir=os.path.join(user_dir,type)
    if not os.path.exists(type_dir):
        os.makedirs(type_dir)
    return type_dir

## method for storing and fetching store network

def removeNetwork(request,name):
    pnm_dir = storageFS(request, 'PNM')
    h5_dir = storageFS(request, 'Networks')
    pnm_path = os.path.join(pnm_dir, name + ".pnm")
    h5_path = os.path.join(h5_dir, name + ".hdf")
    os.remove(pnm_path)
    os.remove(h5_path)


def NetworkStorage(request,name,network=None,object_list=[]):
    network_dir=storageFS(request,'PNM')
    project_name=name
    name=os.path.join(network_dir,name+".pnm")
    workspace={}
    if network == None:
        workspace=pickle.load(open(name,"rb")) ## return network object
        print(workspace[project_name][0])
        return workspace[project_name][0]
    else:
        print(object_list)
        workspace[project_name]=object_list
        pickle.dump(workspace,open(name,"wb"))
        return name
    return

def getProject(request,project_name):
    project = storageFS(request,'PNM')
    return project

def saveProject(request,project_name):
    project = storageFS(request,project_name)

def temp_storage(request,name,network):
    temp_dir = storageFS(request, 'temp')
    name = os.path.join(temp_dir, name + ".pnm")
    pickle.dump(network,open(name,"wb"))
    return name;

def vtkStorage(request,name,network):
    vtk_dir=storageFS(request,'vtk')
    openpnm.export_data(network=network,filename=os.path.join(vtk_dir,name),fileformat='vtk')
    return vtk_dir

## checking json inputs and typecasting strings and intergers
def jsonCheck(json_data):
    data={}
    for key in json_data:
        ## for arrays
        value=json_data[key]
        if value.find("[")!=-1 and value.find("]")!=-1:
            value=value.replace("[","")
            value=value.replace("]","")
            list=value.split(",")
            arr=[]

            for i in list:
                i=i.strip()
                if i.find('.') != -1:
                    v = i.split(".")
                    if v[0].isnumeric() and v[1].isnumeric():
                        arr.append(float(i))
                else:
                    arr.append(int(i))
            data[key]=arr
            print("*********"+key,data[key])

        elif "~" in value:
            data[key] = value
        ## for integer values
        elif value.isnumeric():
            data[key]=int(value)
        ##for float values
        elif value.find('.')!=-1:
            v=value.split(".")
            if v[0].isnumeric() and v[1].isnumeric():
                data[key]=float(value)
            else:
                continue
        elif value.find("True")!=-1 or value.find("False")!=-1:
             data[key]=bool(value)
        ## for None
        elif value.find("None")!=-1:
            data[key]=None
        else:
            data[key]=value
    return data


