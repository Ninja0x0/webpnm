from pymongo import MongoClient
from bson import json_util

import json
import pprint
import numpy as np
import scipy as sp
import scipy.sparse as sprs


class DbConnect:
    def __init__(self):
        self.client = MongoClient("localhost", 27018)

    def connect_db(self, db_name):
        self.db = self.client[db_name]

    def createUser(self, username, email, password):
        users = self.db.users
        user = {"username": username, "email": email, "password": password, "networklist": []}
        users.insert_one(user)
        self.username = username
        self.email = email;

    def findUser(self, *args):
        users = self.db.users
        result = users.find_one({"username": args[0]})
        if result is not None:
            return {"message": "Username already present", "error": "username"}
        result = users.find_one({"email": args[1]})
        if result is not None:
            return {"message": "Email already present", "error": "email"}
        if result is None:
            self.createUser(username=args[0], email=args[1], password=args[2])
            self.username = args[0]
            self.password = args[1]
            self.email = args[2]
            return {"message": "ok", "code": "100"}

        else:
            self.username = args[0];
            self.password = args[1];
            self.email = args[2]

    def loginUser(self, *args):
        users = self.db.users
        result = users.find_one({"username": args[0], "password": args[1]})
        if result is None:
            return {"message": "Username or Password is wrong", "error": "login"}
        else:
            return {"message": "ok", "code": "100"}

    def getUserData(self, *args):
        json_data = {}
        users = self.db.users
        user_collection = users.find_one({"username": args[0], "password": args[1]})
        json_data = json_util.dumps(user_collection)
        return json_data;

    def setSettings(self,credentials,new_settings):
        settings = self.db.settings;
        cred = credentials.copy()
        if "network_name" in cred:
            del cred["network_name"]
        settings.update_one(cred,{'$set':{"data":new_settings}},upsert=True)

    def getSettings(self,credentials):
        settings = self.db.settings
        del credentials["network_name"]
        settings_data=settings.find_one(credentials,{'_id': False})

        return  json_util.dumps(settings_data)


    def addNetwork(self, network_json, credentials):
        # credentials={"username":self.username,"password":self.password}
        self.username = credentials["username"]
        self.password = credentials["password"]
        users = self.db.users
        labels = self.db.labels
        self.createLabels(credentials, network_json)
        user_collection = users.find(credentials)
        for ucollect in user_collection:
            net_collection = users.find({"username": self.username,
                                         "password": self.password,
                                         "networklist.name": {"$eq": network_json["name"]}})
            if net_collection.count() == 0:
                users.update(credentials, {'$push': {'networklist': network_json}})
            else:
                users.update({"username": self.username, "password": self.password,
                              "networklist.name": {"$eq": network_json["name"]}},
                             {'$set': {'networklist.$': network_json}})
                for ncollect in net_collection:
                    pprint.pprint(ncollect)
                    #

        user_collection = users.find(credentials)
        return json_util.dumps(list(user_collection))

    ## save ,remove and  load network in / from database
    def saveProjectInDb(self, credentials, network_json):
        list = self.db.projectslist
        collection = list.find(credentials)
        if collection.count() == 0:
            list.insert_one(credentials)
        list.update_one(credentials, {"$set": {"data": network_json}},upsert=True)

    def loadProjectFromDb(self,credentials):
        list=self.db.projectslist
        collection=list.find(credentials,{"_id": 0, "password": 0})
        return json_util.dumps(collection)

    def removeProjectFromDb(self,credentials):
        list=self.db.projectslist
        print(credentials)
        collection=list.delete_many(credentials)
        print(collection.deleted_count)

    def fetchNetworksList(self, credentials):
        list = self.db.networkslist
        collection = list.find(credentials, {"_id": 0, "password": 0})
        return json_util.dumps(collection)

    def addGeometry(self, credentials, json_data):
        geometry = self.db.geometry;
        data = credentials
        data["network_name"] = json_data["network_name"]
        geometry_collection = geometry.find(data)
        if geometry_collection.count() != 0:
            data["geometry." + json_data["data"]["name"]] = {'$exists': True}
            if geometry.find(data).count() > 0:
                return False
            else:
                del data["geometry." + json_data["data"]["name"]]
                geometry.update(data, {'$set': {'geometry.' + json_data["data"]["name"]: json_data["data"]}})
        else:
            data["geometry"] = {}
            data["geometry"][json_data["data"]["name"]] = json_data["data"]
            geometry.insert_one(data)
        return True

    def createLabels(self, credentials, network_json):
        data = credentials
        labels = self.db.labels
        data["network_name"] = network_json["name"]
        self.network_name = network_json["name"]
        labels.insert_one(data)

    def addNetworkLabels(self, network, credentials):
        labels = self.db.labels
        # remain_pores=self.db.remain_pores
        new_cred = credentials
        new_cred["network_name"] = self.network_name
        for value in network.keys():
            key_name = '_'.join(value.split("."))
            print("keynames", key_name)
            if key_name == "pore_coords":
                labels.update_one(new_cred, {'$set': {key_name: network[value].tolist()}})
            elif key_name == "throat_conns":
                labels.update_one(new_cred, {'$set': {key_name: network[value].tolist()}})
                labels.update_one(new_cred, {'$set': {"throat_lil_conns": self.getLiL(network)}})
            else:
                for value in np.where(network[value]):
                    values = value
                labels.update_one(new_cred, {'$set': {key_name: values.tolist()}})

    ## method used to convert the throat conns lil to pore conns  lil
    def getLiL(self, network):
        conns = network['throat.conns']
        conns = sp.vstack((conns, sp.roll(conns, shift=1, axis=1)))
        am = sprs.coo_matrix((sp.ones(sp.shape(conns)[0]), conns.T))
        am = am.tolil()
        for i in range(am.rows.size):
            am.rows[i].append(i)
        return am.rows.tolist()

    ##
    ##drawbacks loading json and then convertiong to list will be slow for large network
    # def createThroatLabel(self,credentials,pore_name,throat_name,mode):
    #     labels=self.db.labels
    #     projection = {"throat_conns":1}
    #     collection=labels.find_one(credentials,projection)
    #     json_data=json_util.dumps(collection)
    #     json_data = json.loads(json_data)
    #
    #     projection={"$set":{throat_name:values.tolist()}}
    #     labels.update_one(credentials,projection)
    ##
    ##drawbacks loading json and then convertiong to list will be slow for large network
    def createThroatLabelEx(self, credentials, pore_name, throat_name, mode):
        labels = self.db.labels
        match = {"$match": credentials}
        map = {"$map": {"input": "$pore_left",
                        "as": "pores_index",
                        "in": {
                            "$arrayElemAt": ["$throat_lil_conns", "$$pores_index"]}}}
        project = {'$project': {
            "data": map}}  ## include all fields use $addFields command instead of project rest of the things remains same
        reduce = {"$reduce": {"input": "", "initialValue": [], "in": {"$setUnion"}}}

    ##


    ## check whether labels can be attaxched return true if no collison of pores else false
    def checkGeometryHealthEx(self, credentials, project_struct):
        labels = self.db.labels
        match = {'$match': credentials}
        project = {'$project': {"username": 1, "network_name": 1, "values": project_struct}}
        group = {"$group": {"_id": "$network_name", "firstvalues": {"$first": "$values"}}}
        element_check = {"$project": {"values": 1, "present": {"$anyElementTrue": ["$firstvalues"]}}}
        collection = labels.aggregate([match, project, group, element_check])
        json_data = json_util.dumps(collection)
        json_data = json.loads(json_data)
        print(json_data)
        if (json_data[0]["present"]):
            return True
        else:
            return False

    ## check whether any pores are left in the network to assign geometries
    '''
        is different then  checkGeometryHealthEx mehtod
        project strucuture: 
        {"$setIntersection:[$pore_all,{$setUnion:[$label_one ,...]}}}
            
    '''

    def checkGeometryHealth(self, credentials, project_struct):
        labels = self.db.labels
        match = {'$match': credentials}
        project = {'$project': {"username": 1, "network_name": 1, "values": project_struct}}
        group = {"$group": {"_id": "$network_name", "firstvalues": {"$first": "$values"}}}
        element_check = {"$project": {"values": 1, "present": {"$anyElementTrue": ["$firstvalues"]}}}
        labels.aggregate([match, project, group, element_check])
        collection = labels.aggregate([match, project, group, element_check])
        json_data = json_util.dumps(collection)
        json_data = json.loads(json_data)
        if (json_data[0]["present"]):
            return True
        else:
            return False

    ## below method breakpoints
    ## using mutex locks to prevent form accessing temp_labels more than one time  by different users on same id
    ## prevent using credentials from temp labels use esclusion once find the label
    ##
    def applyLogicLabels(self, credentials, label_name, projection_data):
        labels = self.db.labels
        match = {'$match': credentials}
        project = {'$project': {"username": 1, "network_name": 1, label_name: projection_data}}
        out = {"$out": "temp_labels"}
        labels.aggregate([match, project, out])
        temp_labels = self.db.temp_labels
        temp_creds = {"username": credentials["username"], "network_name": credentials["network_name"]}
        labels.update_one(credentials, {"$set": temp_labels.find_one(temp_creds)})
        temp_labels.delete_one(temp_creds)

    def updateGeoData(self, credentials, label_type,label_name):
        projectslist=self.db.projectslist
        creds = {"username": credentials["username"],
                      "network_name": credentials["network_name"],
                      "project_name":credentials["project_name"]}
        projectslist.update_one(creds, {"$addToSet": {'data.geo_data.'+label_type:label_name}})


    def addUserLabels(self, credentials, network_name, label_name, new_index):
        labels = self.db.labels;
        credentials["network_name"] = network_name
        collection = labels.update(credentials, {"$set": {"pore_" + label_name: new_index.tolist()}})
        return list(collection)

    ## return the list of all pore coordinates from mongo database
    def getCoords(self, credentials, network_name):
        labels = self.db.labels
        self.network_name = network_name
        credentials["network_name"] = network_name
        label_collection = labels.find(credentials, {"pore_coords": 1})
        return list(label_collection)

    ## return the array values of specific label
    def getLabelArray(self, credentials, label_name):
        labels = self.db.labels
        label_name = "_".join(label_name.split("."))
        print(label_name)
        label_array = labels.find(credentials, {label_name: 1})
        json_data = json_util.dumps(label_array)
        json_data = json.loads(json_data)
        label_array = np.array(json_data[0][label_name])
        return label_array

    def getKeys(self, keys_json):
        key_names = []
        for key in keys_json:
            key_names.append("$" + '_'.join(key.split(".")))
        return key_names


