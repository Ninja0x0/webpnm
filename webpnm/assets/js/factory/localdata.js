var DashApp=angular.module('FactoryData',[]);

//credentials of user
DashApp.factory("Credentials",function(){
  var credentials={};
  return credentials;
})


DashApp.factory('ProjectsData', function(){
  var data={};
  return data;
});


//models types
DashApp.factory('ModelsData', function(){
  var data={"data":{} ,"info":{},"type":""};
  console.log("modeldata",data);
  data.update=function(value){
    data["data"]=value;
  }
  data.clear=function(value){
    data["data"]={};
    data["info"]={};
    data["type"]=null;
  }
  return data;
});


//geometry types
DashApp.factory('GeoData', function(){
  var data={};
  return data;
});

//containing the network types 
DashApp.factory('NetworkData', function(){
  var data={"data":{}};
  return data;
});

//contining the type of algortihms fetched froms server
DashApp.factory('AlgorithmsData',function(){
   return {
    "data":{}
  };  
});

// storing the state of graph
DashApp.factory('GraphData',function(){
 return {
    "data":{}
  };  
      
});

// this logic visual data
DashApp.factory('LabelGraphData',function(){
   return {
    "data":{}
  }; 
      
});

// this viusal label data
DashApp.factory('LabelData',function(){
   return {
    "data":{}
  }; 
});

DashApp.factory('SettingsData', function(){
  var data={};
  return data;
});


// used for copying and pasting models from one geometry to another
DashApp.factory('ClipBoard',function(){
  var data={"type":{},"data":{}};
  data.update=function(type,value){
    data["type"]=type;
    data["data"]=value;
  }
  return data;  
});
