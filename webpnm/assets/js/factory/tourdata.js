var DashApp=angular.module('TourData',[]);
DashApp.factory("Tour",function(){
	var data={};
	data["tour"] = {
            id: "project-tour",
            steps: [
              {
                title: "Project",
                content: "Create an project in webpnm",
                target: "settings_icon",
                placement: "right"
              },
              {
                title: "My content",
                content: "Here is where I put my content.",
                target: document.querySelector("#create_icon"),
                placement: "bottom"
              }
            ]
          };

	  data["network_tour"] = {
	    id: "network-tour",
	    steps: [
	      {
	        title: "Create Network",
	        content: "Create an network in webpnm",
	        target: document.querySelector("#tour02"),
	        placement: "right"
	      }
	    ]
	  };


	return data;




})