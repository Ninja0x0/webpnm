angular.module("DashApp").directive('appendDropdown', function(ProjectsData,General){
    return {
      restrict: 'A',
       
    
      link:function(scope,element,attr){
        var popup_array=["geometry_popup","phase_popup","physics_popup"];
        var popup_create=["geometry_create","phase_create"]        

        var generateName=function(data,prefix){
          var name = null;
          var flag = true;
          while(true){
            name = [prefix,"_",Math.floor(Math.random()*1000)].join("");
            for(key in data){       
              if(key == name) {
                flag = false;
                break;
              }
            }
            if(flag) break;
          }    

          return name;    
        }

       


        element.on('click',function(event){

           
              $("#"+attr.appendDropdown).css("display","block");            
              var text_node = $("#"+attr.appendDropdown+" input[type='text']")
              var network = General.getNetworkInfo()
              var data=ProjectsData[General.project_name][network.name];      
              if(attr.appendDropdown == "modal_geometry"){
                  text_node.val(generateName(data,"geo"));
                  angular.element(text_node).triggerHandler('input'); 
                  
              }else if (attr.appendDropdown == "modal_phases") {              
                  text_node.val(generateName(data,"phases"));
                  angular.element(text_node).triggerHandler('input'); 
                   
              }else{
                  text_node.val(generateName(data,"physics"));
                  angular.element(text_node).triggerHandler('input'); 
                  
              }
            
            
           

        });
        window.onclick = function(event) {
          var geo_modal=$("#modal_geometry");
          var physics_modal=$("#modal_physics");
          var phases_modal=$("#modal_phases");         
          
          if (event.target == geo_modal[0]) {
              $("#modal_geometry").css("display","none");

          }else if(event.target == physics_modal[0]){
              $("#modal_physics").css("display","none");
          }else if(event.target == phases_modal[0]){
              $("#modal_phases").css("display","none");
          }
        }  
        
        Array.prototype.contains=function(value){
            for(i in this){    
             
              if(value == this[i]) return true;
            }
            return false;
        }

    .onclick = function(event) {
           
            for(class_name in popup_array){
                container=$("."+class_name);
              
                if(!popup_create.contains($(event.target).attr('class'))){
                 
                   if (!container.is(event.target) && container.has(event.target))  {
                     
                     var flag=container.hasClass("show")
                     if(flag)
                        container.removeClass("show");
                   
                  }
                }
               
            }           
          
        }



        
      }
     
    }
  }); 