angular.module("DashApp").directive("uploadFile",function(ServerCalls){
  return {
    restrict:'A',    
    link:function(scope,element,attrs){        
        var file=null;        
      
        var validation =function(project_name){
             if(/^[a-zA-Z0-9]*$/.test(project_name)){
                return true;
             }else{
                return false; 
             }
        }

        var upload=function(formdata){
            var request = new XMLHttpRequest();
            request.open("POST", "/api/upload/");
            request.send(formdata);
            request.onreadystatechange = function() {
                if (this.readyState == 4) {
                    window.location="/web/dashboard/";  
               }
            };
        }
        
       var runForm=function(file){        
          var project_name=file.name.split(".")[0];
          if(!validation(project_name)){
            ServerCalls.messageError("","File name should not contain special character");
          }else{
            var formdata=new FormData();
            formdata.append("type","pnm");
            formdata.append("pnmfile",file);
            formdata.append("file-name",project_name);
            formdata.append("csrfmiddlewaretoken",ServerCalls.getCookie("csrftoken"));
            upload(formdata);
          }
       }
        
        element.bind("dragover",function(ev){         
            ev.preventDefault();                    
            $(".drag_column").css('border-color','#0b7850');
            $(".upload_pnm").css('background','white');
           
         
        })
        element.bind("dragleave",function(ev){         
            ev.preventDefault();            
            $(".drag_column").css('border-color','#9e9e9e');
            $(".upload_pnm").css('background','whitesmoke');
            
        })
        element.bind("input",function(ev){
                   
          file = element[0].files[0];                
          runForm(file);
            
        })

        element.bind("drop",function(ev){        
          file=ev.dataTransfer.files[0];      
          runForm(file);
          ev.stopPropagation();
          ev.preventDefault();        
         
        });
    	 

        
    	


    }
      
    
  }
});
