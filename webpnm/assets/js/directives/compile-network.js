angular.module("DashApp").directive("compileNetwork",function(ServerCalls,General,LabelGraphData,GraphData){
  return {
    restrict:'A',
    scope:{
    	project: "=",
    },
    link:function(scope,element,attrs){
    	
    	var graph=null;

    	var graphNode=function(graph){
              	graph["nodes"].forEach(function(item, index, array) {                
	                node=new LGraphNode();
	                node.configure(graph["nodes"][index])
	                graph.add(node);
        		});
        }

    	var getTargetID=function(target_id,target_data){
              target_data=target_data || {}
              var node=graph.getNodeById(target_id);
			  target_data[node.title]={}              
              for(key in graph.links){
                
                obj=graph.links[key]
                if(obj.target_id == target_id && (node.type == "logic" || node.type == "throat_logic")){            
                  getTargetID(obj.origin_id,target_data[node.title])
                }
                else if(obj.target_id == target_id) {
                  target_data[node.title]=undefined                 
                }
              }
              return target_data;
              
              
            }

        var getSimTargetID=function(target_id,target_data){
          target_data=target_data || {}
          var node=graph.getNodeById(target_id);
          target_data[node.title]=[] 
              
          for(key in graph.links){
            
            obj=graph.links[key]
            if(obj.target_id == target_id){

                  c_node=graph.getNodeById(obj.origin_id) 
                  if( c_node.type== "method"){
                    data={}
                    for(index in c_node.inputs){
                        input_name=c_node.inputs[index].name
                        input_node=c_node.getInputNode(index)
                        if(input_node != null){
                          if(input_node.type == "pores"){

                            data[input_name]=(input_node.title.split("_")).join(".")
                          }
                          else{
                            data[input_name]=input_node.title
                          }
                          
                        }                                                     
                          
                            
                    }
                    target_data[node.title].push({[c_node.title]:data});
                  }                    
                  else{
                    getTargetID(obj.origin_id,target_data[node.title])
                  }                  
            }
            
          }
          return target_data;       
          
        }
         var getNodes=function(nodes){
         	var labels_collection=[]
    	
         	for(index in nodes){
         		obj=nodes[index]
         		if(obj != undefined){
	         		if(obj.type == "message"){	         			
	         			node=graph.getNodeById(obj.id);
	         			logic=node.getInputNode(0);
	         			values=getTargetID(logic.id);
	         			o={}
	         			console.log(obj)
	         			o[node.title]=values
	         			labels_collection.push(o)
	         			// for(index in nodes){
	         			// 	if(nodes[index].type == "message"){
	         			// 		o={}
	         			// 		o[nodes[index].title]=values
	         			// 		labels_collection.push(o)
	         			// 	}
	         			// }
	         		}
         		}	         	
         	}
         	return labels_collection;
         	
         }

         var getSimulationNodes=function(nodes){         	
    		var sim_collection=[]
         	for(index in nodes){
         		obj=nodes[index]         		
         		if(obj != undefined){
	         		if(obj.type == "algorithim"){
	         			values=getSimTargetID(obj.id);         			
	         			sim_collection.push(values);        			
	         			
	         		}
	         	}	         	
         	}
         	return sim_collection;
         }


    	element.bind("click",function(){
    		console.log(scope.project);
    		graph=new LGraph();
    		var network_data=General.getNetworkInfo();
    		if(LabelGraphData["data"][scope.project] != undefined){
		    	graph.configure(LabelGraphData["data"][scope.project][network_data.name]);
		    	graphNode(graph)
		    	console.log(graph)
	        	network_data["label_collection"]=getNodes(graph["nodes"])
	        }

	        graph=new LGraph();
	        if(GraphData["data"][scope.project] != undefined){
		        graph.configure(GraphData["data"][scope.project][network_data.name]);
		        graphNode(graph)
		        network_data["simulation_collection"]=getSimulationNodes(graph["nodes"])
	    	}
	       	ServerCalls.downloadScript(scope.project);
    	});
    	
     	
      
    }
  }
});
