angular.module('LoginApp').directive("validation",function(){
    return {
        link:function(scope,element,attrs){
                scope.validation=function(){
                if(scope.username.length <6 ){
                    $("#username").tooltip({tooltip:"Minimium 6 characters long",delay:500})
                    return false;
                }
                if(scope.password.length < 8 ){
                    $("#password").tooltip({tooltip:"Minimium 8 characters long",delay:500})
                    return false;
                }

            }
        }
    }
});
