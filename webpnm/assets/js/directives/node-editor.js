angular.module("DashApp").directive("nodeEditor",function(LabelGraphData){
   return{
    restrict:'EA',    
    link:function(scope,element,attrs){ 

            $('.ui.dropdown').dropdown();
            el=$("main")
            $("canvas").attr("height",el[0].offsetHeight-(el[0].offsetHeight*5)/100);
            $("canvas").attr("width",el[0].offsetWidth);
            $("canvas").on("click",function(){
              $("canvas").focus();
            })
            $(".name").on('click',function(){
                
                 var parent=$(this).parent();
                 var menu=$(this).parent().find(".menu");
                 var mouseout=function(event) {
                   
                    var e = event.toElement || event.relatedTarget;
                    
                    if (e.parentNode == this || e == this || $.contains(this,e)) {                      
                       return;
                    }
                    menu.css('display','none');
                    menu.css('top',event.offsetY);
                    menu.css('left',event.offsetX);           
                 }   
                 menu.css('display','block');
                 parent.on('mouseout',mouseout);
            })
            




            var logic=scope.logic;
            var network_name=logic.network_name;
            var project_name=logic.project_name;
            var graph = null;
            console.log(LabelGraphData)
            if(project_name in  LabelGraphData["data"]) {
              graph=new LGraph()              
              graph.configure(LabelGraphData["data"][project_name][network_name]);              
              graphNode(graph);             
            }else{ 
              graph=new LGraph();
            }
           

            var canvas = new LGraphCanvas("#editor",graph);
            
            console.log(graph);
            canvas.processHTMLContextMenu=function(n,e){
              console.log(n);
              console.log(e);
              e.preventDefault();
              if(n instanceof LGraphNode){
                  if(n.type == "logic" || n.type.includes("pore_logic")){
                    $("#logic-menu").css('display','block'); 
                    console.log(e);                                 
                    $("#throat-menu").css('display','block');                             
                    $("#throat-menu").css('display','none');
                  }else if( n.type.includes("throat_logic")   ){
                    $("#throat-menu").css('display','block');               
                    $("#logic-menu").css('display','none');
                       

                  }

                  else{
                    $("#logic-menu").css('display','none');
                    $("#throat-menu").css('display','none');
                  }
                  $("#node-menu").css('display','block');              
                  $("#node-menu").css('left',e.pageX)
                  $("#node-menu").css('top',e.pageY)
                  contextMenuNode=n;
              }
              else{
                  $("#right-menu").css('display','block');              
                  $("#right-menu").css('left',e.pageX)
                  $("#right-menu").css('top',e.pageY)
              }
             contextMenuEvent=e;

            }

            // this callback is used to set localdata every time mouse enter the node (***)
            var onConnectionsChange = function(){
              
              graph.updateExecutionOrder();
              graph.change();           
              logic.saveLocalData(graph.serialize())
            }

            var onQueryConnection = function(){
              console.log(this)
              var input_id = this.getInputInfo(Number(0));
              input_id = graph.links[input_id.link]
              if(input_id!=undefined){
                input_id = input_id.origin_id;
                var input_node = graph.getNodeById(input_id);
                if(this.type == "throat_logic"){
                  if(input_node.type!="pores"){                                    
                    this.disconnectInput(Number(0));
                  }else{
                    this.input_type = input_node.type;
                  }
                }else{
                  if(["pores","throats"].indexOf(input_node.type)<0){                  
                    this.disconnectInput(Number(0));
                  }else{
                    this.input_type = input_node.type;
                  }
                }
              }          

            }

            
            var onMouseDown=function(){
              console.log(this)
            }
            var onMouseUp=function(){
               console.log(this)
            }
           

            $("#right-menu , #node-menu").mouseleave(function(){
              $("#right-menu,#node-menu").css('display','none');
            });
            $("#right-menu , #node-menu").bind('contextmenu',function(e){
                return false;
            });


            
            var getTarget=function(){
            var target={};

              for(key in graph.links){
                target[graph.links[key].target_id]=[];
              }

              for(key in graph.links){
                var t=graph.links[key].target_id;
                var o=graph.links[key].origin_id;
                target[t].push(o);
              }
              return target;
            }

            var getTargetID=function(target_id,target_data){
              target_data=target_data || {}
              var node=graph.getNodeById(target_id);
              target_data[node.title]={}
              console.log(node.id)
              for(key in graph.links){
                
                obj=graph.links[key]
                if(obj.target_id == target_id && (node.type == "logic" || node.type == "throat_logic" || node.type == "pore_logic")){            
                  getTargetID(obj.origin_id,target_data[node.title])
                }
                else if(obj.target_id == target_id) {
                  target_data[node.title]=undefined                 
                }
              }
              return target_data;
              
              
            }



            scope.getTargetsByName=function(){
              var target=getTarget();
              console.log(target);
              var new_list={};
              for(key in target){
                var node=graph.getNodeById(key)
                console.log(node)
                new_list[node.title]=[]

                for(index in target[key]){
                  var link_node=graph.getNodeById(target[key][index])
                  console.log("linke_node",link_node)
                  new_list[node.title].push(link_node.title)
                }
              }
              return new_list;
            }            



            scope.getLabelData=function(){
              return getTargetID(contextMenuNode.id);
            }
            scope.getNode=function(){
              return contextMenuNode;
            }
            
            scope.addAjaxPoreNode=function(name){
              var flag_addNode=true;
              for(key in contextMenuNode.getOutputNodes(0)){                 
                  if(contextMenuNode.getOutputNodes(0)[key].title == name){
                    flag_addNode=false;
                    break;
                  }
              }
              if(flag_addNode){
                var node = new LGraphNode(name)
                node.pos[0]=contextMenuEvent.offsetX+(Math.random()*150);
                node.pos[1]=contextMenuEvent.offsetY+(Math.random()*150);                          
                node.type="message";
                node.addInput("IN","string");
                node.addOutput("OUT","string");               
                node.onConnectionsChange =onConnectionsChange ;
                graph.add(node);
                node.title=name;
                contextMenuNode.connect("OUT",node,"IN");
                logic.saveLocalData(graph.serialize())
              }
            }
           

            scope.addPoresLabel=function(name){
              var node = new LGraphNode(name)
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addOutput("OUT","string");             
              node.type="pores";
              node.onConnectionsChange =onConnectionsChange ; 
              
              graph.add(node);
              node.title=name;
              logic.saveLocalData(graph.serialize())
            }
            scope.addThroatLabel=function(name){
              var node = new LGraphNode(name)
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addOutput("OUT","string");             
              node.type="throats";
              node.onConnectionsChange =onConnectionsChange ; 
              
              graph.add(node);
              node.title=name;
              logic.saveLocalData(graph.serialize())
            }
          
            scope.addQueryNode=function(name,type){
              var node = new LGraphNode(name)
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addOutput("OUT","string");
              node.addInputs([["pore/throat","pores"]])          
              node.type=type;
              node["input_type"]=null;
              node.onConnectionsChange =onQueryConnection;             
              graph.add(node);
              node.title=name;
              logic.saveLocalData(graph.serialize())
            }

             scope.addThroatNode=function(name){
              var node = new LGraphNode(name)
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addOutput("OUT","string");
              node.addInputs([["IN 2","number"]])          
              node.type="throat_logic";
              node.onConnectionsChange =onConnectionsChange ;
              graph.add(node);
              node.title="find_neighbor_"+name;
              logic.saveLocalData(graph.serialize())
            }

            scope.addLogicNode=function(name){
              var node = new LGraphNode(name)
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addInputs([["IN 1","number"]])
              node.addOutput("OUT","string");
              node.type="logic";
              node.onConnectionsChange =onNodeAttach ;
              node["uIncrement"]=1
              node.onMouseUp=onMouseUp;            
              graph.add(node);
              node.title=name;
              logic.saveLocalData(graph.serialize())
            }
            var onNodeAttach=function(){              
              var flag_noinputs=true;
              for(key in this.inputs){                
                if(!key.includes("contains")){
                  console.log(this.inputs[key])
                  if(!this.isInputConnected(key)){
                    flag_noinputs=false;
                    break;
                  }
                }
              }
              if(flag_noinputs){
                this.uIncrement++;
                this.addInput("IN "+this.uIncrement,"number")
              }

              graph.updateExecutionOrder();
              graph.change();           
              logic.saveLocalData(graph.serialize())
            }

            scope.removeNode=function(){
              graph.remove(contextMenuNode);
              $("#right-menu,#node-menu").css('display','none');
              logic.saveLocalData(graph.serialize())
            }
            scope.clear=function(){
              graph.clear();
              $("#right-menu,#node-menu").css('display','none');
              logic.saveLocalData(graph.serialize())
            }

            scope.resetZoom=function(){
              canvas.scale=1;
              canvas.offset=[0,0];
              canvas.graph.change();              
            }
            // this common function used in simulation as well
             function graphNode(graph){
              graph["nodes"].forEach(function(item, index, array) {                
                node=new LGraphNode();
                node.configure(graph["nodes"][index])
                graph.add(node);
              });
            }     
          
            scope.showModal=function(){
              $("#modal_design").css("display","block");              
              window.onclick = function(event) {
                var modal=$("#modal_design");                
                if (event.target == modal[0]) {
                    $("#modal_design").css("display","none");
                }
              }  

            }
            scope.showLabelModal = function(){            
              window.onclick = function(event){
                var modal = $("#label_modal");               
                if (event.target == modal[0]) {                   
                     scope.logic.label_modal = false;
                     scope.$apply();
                }
              }
            }
            
                       

            
            
    }
  }
});