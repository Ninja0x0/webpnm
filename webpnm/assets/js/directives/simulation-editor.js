angular.module("DashApp").directive("simulationEditor",function(ProjectsData,AlgorithmsData,GraphData){
  return{
      link:function(scope,element,attrs){

            $('.ui.dropdown').dropdown();
            el=$("main")
            $("canvas").attr("height",el[0].offsetHeight-(el[0].offsetHeight*5)/100);
            $("canvas").attr("width",el[0].offsetWidth);
            $("canvas").on("click",function(){
              $("canvas").focus();
            })
           // used for creating popup on canvas
            // $(".name").on('click',function(ev){
                 
            //      var parent=$(this).parent();
            //      var menu=$(this).parent().find(".menu");
            //      var mouseout=function(event) {
                   
            //         var e = event.toElement || event.relatedTarget;
                    
            //         if (e.parentNode == this || e == this || $.contains(this,e)) {                      
            //            return;
            //         }
            //         menu.css('display','none');
            //         menu.css('top',ev.originalEvent.offsetY);
            //         menu.css('left',(ev.originalEvent.offsetX+70));         
            //      }   
            //      menu.css('display','block');
            //      menu.css('top',ev.originalEvent.offsetY);
            //      menu.css('left',(ev.originalEvent.offsetX+70));     
            //      parent.on('mouseout',mouseout);
            // })

            var simulation=scope.simulation;
            var network_name=simulation.network_name;
            var project_name=simulation.project_name;
            var phases_list=ProjectsData[project_name][network_name]["phases"];
            var graph=null;
            if(project_name in  GraphData["data"]) {
              graph=new LGraph()
              graph.configure(GraphData["data"][project_name][network_name]);              
              graphNode(graph);
            }
            else 
              graph=new LGraph();
           
           
              
            var canvas = new LGraphCanvas("#simulation-editor",graph);
            console.log(canvas);
                
            simulation.GRAPH=graph;

            var contextMenuNode=null;
            var contextMenuEvent=null;
            canvas.processHTMLContextMenu=function(n,e){
             
              // e.preventDefault();
              if(n instanceof LGraphNode){                
                  $("#node-menu").css('display','block');              
                  $("#node-menu").css('left',e.pageX)
                  $("#node-menu").css('top',e.pageY)
                  contextMenuNode=n;
                  simulation.type=n.title;
                  scope.$apply();
              }
              else{
                  $("#right-menu").css('display','block');              
                  $("#right-menu").css('left',e.pageX)
                  $("#right-menu").css('top',e.pageY)

              }
             contextMenuEvent=e;

            }



            $("#right-menu , #node-menu").mouseleave(function(){
              $("#right-menu,#node-menu").css('display','none');
            });
            $("#right-menu , #node-menu").bind('contextmenu',function(e){
                return false;
            });
            

            


            var getTargetsByName=function(){
              var target=getTarget();
              var new_list=[];              

              for(key in target){
                var connect_object={name:"",type:"",connect_to:"",connect_type:""}
                var node=graph.getNodeById(key)
                connect_object["name"]=node.title;
                connect_object["type"]=node.type;
                console.log(connect_object)

                for(index in target[key]){
                  var link_node=graph.getNodeById(target[key][index])
                  connect_object["connect_to"]=link_node.title;
                  connect_object["connect_type"]=link_node.type;
                  new_list.push(connect_object);
                }
              }
              return new_list;
            }

            var getTargetID=function(target_id,target_data){
              target_data=target_data || {}
              var node=graph.getNodeById(target_id);
              target_data[node.title]=[] 
                  
              for(key in graph.links){
                
                obj=graph.links[key]
                if(obj.target_id == target_id){

                      c_node=graph.getNodeById(obj.origin_id) 
                      if( c_node.type== "method"){
                        data={}
                        for(index in c_node.inputs){
                            input_name=c_node.inputs[index].name
                            input_node=c_node.getInputNode(index)
                            if(input_node != null){
                              if(input_node.type == "pores"){

                                data[input_name]=(input_node.title.split("_")).join(".")
                              }
                              else{
                                data[input_name]=input_node.title
                              }
                              
                            }                                                     
                              
                                
                        }
                        target_data[node.title].push({[c_node.title]:data});
                      }                    
                      else{
                        getTargetID(obj.origin_id,target_data[node.title])
                      }                  
                }
                
              }
              return target_data;       
              
            }

            
//  using scope.apply multiple times is good approacjh ??? do some research
            scope.getAlgorithmData=function(){              
               return getTargetID(contextMenuNode.id)
            }

            
            
            // save when connection change on node
            var onConnectionsChange =function(){
              graph.updateExecutionOrder();
              graph.change();             
              simulation.saveLocalData(graph.serialize())
            }
            

          
            

            scope.returnSelectedNode=function(){
              return contextMenuNode;
            }

            scope.addAlgorithm=function(name){              
              var node = new LGraphNode(name);              
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;              
              node.onConnectionsChange=onConnectionsChange;
             
              node.type="algorithim";
              node["attached_methods"]={}
              graph.add(node);
              node.title=name;
              simulation.saveLocalData(graph.serialize())
              
            }
            scope.addPhasesNode=function(name){
              
              var node = new LGraphNode(name);
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              node.addOutput(name,"string");
              node.type="phase";
              node.onConnectionsChange=onConnectionsChange;
              
              graph.add(node);              
              node.title=name;
             
              //var x=node.connect(name,contextMenuNode,name+" input");
              //console.log(x);
              simulation.saveLocalData(graph.serialize())
              
            }

            //dependent node on the type of algorithm
            scope.addMethodNode=function(name){
              var parameters=simulation.getMethodParamters(name);
              var node = new LGraphNode(name);
              node.pos[0]=contextMenuEvent.offsetX-(Math.random()*150+100);
              node.pos[1]=contextMenuEvent.offsetY-(Math.random()*150+100);
              for(key in parameters){
                node.addInput(key,"string")
              }
              node.addOutput("out","string")
              node.type="method"
              node.onConnectionsChange=onConnectionsChange;
              
              graph.add(node);
              node.title=name;
              contextMenuNode.addInput(name)
              var x=node.connect("out",contextMenuNode,name);
              console.log(x)
              simulation.saveLocalData(graph.serialize())
              
            }

            scope.addPoresNode=function(name){              
              var node = new LGraphNode(name);
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;              
              node.addOutput("out","string")
              node.type="pores"
              node.onConnectionsChange=onConnectionsChange;
            
              graph.add(node);
              node.title=name;
              simulation.saveLocalData(graph.serialize())
                      
            }

            scope.addNode=function(name,type,inputs){
              var node = new LGraphNode(name);
              node.pos[0]=contextMenuEvent.offsetX;
              node.pos[1]=contextMenuEvent.offsetY;
              console.log("inputs",inputs)
              for(var i=0;i<inputs;i++){
                if(input == 1)
                  node.addInput("IN","string")
                else
                  node.addInput("IN "+(i+1),"string")
              }                   
              node.addOutput("out","string")
              node.type=type
              node.onConnectionsChange=onConnectionsChange;
              graph.add(node);
              node.title=name;
              simulation.saveLocalData(graph.serialize())
            }

            // proerties and remove right click of node configuration
            scope.resetZoom=function(){
              canvas.scale=1;
              canvas.offset=[0,0];
              canvas.graph.change();              
            }      
            scope.remove=function(){
              graph.remove(contextMenuNode)
              graph.updateExecutionOrder();
            }

            function graphNode(graph){
              graph["nodes"].forEach(function(item, index, array) {                
                node=new LGraphNode();
                node.configure(graph["nodes"][index])
                graph.add(node);
              });
            }     

          console.log(canvas)
         
                 
            

     

           


      }
  }

});
