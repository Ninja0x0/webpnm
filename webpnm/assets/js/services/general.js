angular.module("DashApp").service("General",function(ProjectsData){
  this.project_name=null;


  this.getNetworkInfo=function(){
    var general=this;
    var value=ProjectsData[general.project_name];   
    for(x in value){
     return value[x];
    }
    return;
  }
  
  this.objAlreadyExist=function(name){
    var general = this;
    var value =ProjectsData[general.project_name];   
    for(key in value["geometries"]){
      if(key == name) return true;
    }
    for(key in value["phases"]){
      if(key == name) return true;
    }
    for(key in value["physics"]){
      if(key == name) return true;
    }
    return false;

  }

  this.validation=function(name){
     var regex = new RegExp("^[a-zA-Z0-9_]*$");
     if(regex.test(name)) return true;
     else return false;
  }

  
  
});