// list of xhr calls 
angular.module("ServerCallsModule",["ngAlertify"]).service("ServerCalls",function($http,SettingsData,Credentials,ProjectsData,NetworkData,ModelsData,GeoData,AlgorithmsData,General,alertify){    
 
    this.types={};  
    this.modelsdata=ModelsData["data"];
    this.project_name=null;
    this.loadNetworkDB=function(network){
    cookie_name=network.getCookie("csrftoken");

    $http({
                method : "POST",
                url : "/api/load",
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
                
                if(response.data["message"]=="redirect")
                  window.location=response.data["path"]
                else{
                  Credentials=response.headers();
                  network.parseProject(response.data);               
                  network.spinner=false;
                }
                
                   

            }, function myError(response) {

               console.log("error",response);
            });
    }

    this.getTypeData=function(type){         
        cookie_name=this.getCookie("csrftoken");
        scope=this;
        $http({
                method : "POST",
                url : "/api/types/"+type,
                headers:{                  
                  "X-CSRFToken":cookie_name
                },                
            }).then(function mySuccess(response) {
                scope.types[type]=response.data;                                

            }, function myError(response) {
                server.parseError(response);
            });        
    }
    this.getCookie=function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    this.getBuiltInModels=function(type,data){
      var server = this;
      var network_data=General.getNetworkInfo();      
      var network_name=network_data.name;     
      
      if(type.toLowerCase() =='geometry'){           
           geometry_name=data["name"];                    
           network_data["geometries"][geometry_name]=data;
           built_models=network_data["geometries"][geometry_name];
      }
      else if(type.toLowerCase() =='phases' || type.toLowerCase() == "phase"){        
        phase_name=data["name"];       
        network_data["phases"][phase_name]=data;
        built_models=network_data["phases"][phase_name];
        type="phases";    
      }
      else if(type.toLowerCase() =='physics'){        
        physics_name=data["name"]; 
        network_data["physics"][physics_name]=data;
        built_models=network_data["physics"][physics_name];      
      }
      sub_type=data.type;
      var url=[type.toLowerCase(),sub_type].join("/")
      
      $http({
            method : "POST",
            url : "/api/attach/"+url,
            headers:{
              "Content-Type":"application/text",
              "X-CSRFToken":cookie_name
            },
         
        }).then(function mySuccess(response) {
            
            built_models["built_models"]=Object.assign({},response.data);               
        }, function myError(response) {
           server.parseError(response);
        });
    }

    this.getModels=function(type,network_name,type_name){
      var server=this;
      ModelsData["info"][type]=type_name;
      ModelsData["info"]["Network"]=network_name;
      ModelsData["type"]=type;
      cookie_name=this.getCookie("csrftoken");    
      $http({
                  method : "POST",
                  url : "/api/models/"+type+"/",
                  headers:{
                    "Content-Type":"application/json",
                    "X-CSRFToken":cookie_name
                  },
                  data:""
              }).then(function mySuccess(response) {   
                  server.getMisc(type,network_name,type_name);
                  ModelsData["data"]=response.data;
                  

                  

              }, function myError(response) {
                 server.parseError(response);
              });
    }
    this.getMisc=function(type,network_name,type_name){
         
          ModelsData["info"][type]=type_name;
          ModelsData["info"]["Network"]=network_name;
          ModelsData["type"]=type;
          cookie_name=this.getCookie("csrftoken");    
          $http({
                      method : "POST",
                      url : "/api/models/misc/",
                      headers:{
                        "Content-Type":"application/json",
                        "X-CSRFToken":cookie_name
                      },
                      data:""
                  }).then(function mySuccess(response) {
                      data=response.data;
                      ModelsData["data"]["misc"]=data["misc"];                      
                      

                  }, function myError(response) {
                      server.parseError(response);
                  });
        }

    

    this.fetchTypes=function(type,scope=null){    
    cookie_name=this.getCookie("csrftoken");

    $http({
                method : "POST",
                url : "/api/types/"+type,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
              type=type.toLowerCase();
              
              if(type == "network"){                
                NetworkData["data"]=response.data;
              }
              
                
              console.log(AlgorithmsData)
            }, function myError(response) {
               server.parseError(response);
            });


  }
  this.createNetwork=function(main,network,network_data=null){
    var server=this;
    cookie_name=this.getCookie("csrftoken");
    network_data=network.applyNetwork();    
    project_name=network.open_project;
     
    $http({
                method : "POST",
                url : "/api/project/"+network.open_project,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
            }).then(function mySuccess(response) {
                network.insertNetworkIntoProject(project_name,{[network_data.name]:network_data}); 
                data=JSON.parse(JSON.stringify(response.data));
                network_name=data["network_name"]
                project_name=data["project_name"]
                GeoData[project_name]={}
                GeoData[project_name][network_name]={}
                GeoData[project_name][network_name]["attached_pores"]={}
                GeoData[project_name][network_name]["attached_throats"]={}               
                GeoData[project_name][network_name]["pores"]=data["pores"]
                GeoData[project_name][network_name]["throats"]=data["throats"]
                
                
                ProjectsData[project_name][network_name]["geo_data"]=GeoData[project_name][network_name]
                
                

                // network.insertNetworkIntoProject(project_name,{[network_name]:networks_data});
                network.hide_table=false;
                network.clearInputs();
                
                main.networks_data=General.getNetworkInfo();
                main.network_name=main.networks_data.name;

            }, function myError(response) {
               server.parseError(response);
            });
   }

  this.saveToDB=function(project_name,network_data){
    var server=this;
    cookie_name=server.getCookie("csrftoken");    
    var credentials={username:Credentials["username"],password:Credentials["password"]};
    server.messageAlert("Saving ....")

    $http({
                method : "POST",
                url : "/api/save/"+project_name,
                params:credentials,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
            }).then(function mySuccess(response) {
               
                server.messageAlert("Data Saved");
                

            }, function myError(response) {
                server.parseError(response);
            });
  }

   this.getHelpString=function(module_name,class_name){
    var server =  this;
      cookie_name=this.getCookie("csrftoken");
      data={"module":module_name,"class":class_name}
      $http({
                method : "POST",
                url : "/api/help",
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                params:data
            }).then(function mySuccess(response) {

                var ajax_response = $(".help");
                var heading = $(".help > .heading");
                var description = $(".help > .description");
                ajax_response.css('display','inline-grid');
                ajax_response.fadeIn();                
                heading.text(class_name);
                description.html(Prism.highlight(response.data, Prism.languages.python));
                setTimeout(function(){ajax_response.fadeOut() }, 4000);

            }, function myError(response) {
               server.parseError(response);
            });
    }
    this.applyLabels=function(project_name){
      cookie_name=this.getCookie("csrftoken");
      data=LabelData["data"];
      $http({
                method : "POST",
                url : "/api/labels/"+network_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:data
            }).then(function mySuccess(response) {
                
                for(key in response.data){
                  console.log(key);
                  GeoData["data"][network_name]["pores"].push("pore."+key)
                  GeoData["data"][network_name]["pore_health"].push("pore."+key)
                  ResultData["data"][network_name]["labels"]["pore."+key]=response.data[key];
                  console.log(GeoData["data"][network_name]["pores"])
                }
                                


            }, function myError(response) {
               server.parseError(response);
            });
    }
    this.applyLogicLabels=function(scope,project_name,label_type,label_name,logic_data){
      network=General.getNetworkInfo();
      cookie_name=this.getCookie("csrftoken");
      logic_data["network_name"]=network.name;
      logic_data["label_type"]=label_type;
      logic_data["label_name"]=label_name;      
      $http({
                method : "POST",
                url : "/api/logiclabels/"+project_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:logic_data
            }).then(function mySuccess(response) {
                //create a node of label name attach the algorithim which created it
                var data=response.data;
                var geo_array=GeoData[data.project_name][data.network_name][data.type];
                if(!geo_array.includes(data.label_name))
                  GeoData[data.project_name][data.network_name][data.type].push(data.label_name)
                     
                scope.addAjaxPoreNode(data.label_name)                              

            }, function myError(response) {
               server.parseError(response);
            });
    }

    this.applyPoreLabels=function(scope,project_name,label_name,data,input_type){
      cookie_name=this.getCookie("csrftoken");
      network=General.getNetworkInfo();
      var json_data={}
      json_data=data;
      json_data["network_name"]=network.name;

      $http({
                method : "POST",
                url : "/api/porelabels/"+project_name+"/"+label_name+"/"+input_type,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:json_data
            }).then(function mySuccess(response) {
                //create a node of label name attach the algorithim which created it
               var data=response.data
               var geo_array=GeoData[data.project_name][data.network_name][data.type];
               if(!geo_array.includes(data.label_name))
                  GeoData[data.project_name][data.network_name][data.type].push(data.label_name)
               scope.addAjaxPoreNode(data.label_name)                 

            }, function myError(response) {
                server.parseError(response);
            });

      
    }

    this.applyThroatLabels=function(scope,project_name,label_name,data){
      cookie_name=this.getCookie("csrftoken");
      network=General.getNetworkInfo();
      var json_data={}
      json_data[label_name]=data
      json_data["network_name"]=network.name;

      $http({
                method : "POST",
                url : "/api/throatlabels/"+project_name+"/"+label_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:json_data
            }).then(function mySuccess(response) {
                //create a node of label name attach the algorithim which created it
               var data=response.data
               var geo_array=GeoData[data.project_name][data.network_name][data.type];
               if(!geo_array.includes(data.label_name))
                  GeoData[data.project_name][data.network_name][data.type].push(data.label_name)
               scope.addAjaxPoreNode(data.label_name)
                              

            }, function myError(response) {
                server.parseError(response);
            });


    }

    this.applyLogicConditions=function(logic,scope,project_name,label_name,conditons){
        cookie_name=this.getCookie("csrftoken");
        network = General.getNetworkInfo();
        label_name=["pore",label_name].join("_");
        $http({
                  method : "POST",
                  url : "/api/logiccondition/"+project_name+"/"+network.name+"/"+label_name,
                  headers:{
                    "Content-Type":"application/text",
                    "X-CSRFToken":cookie_name
                  },
                  data:conditons
              }).then(function mySuccess(response) {
                  var data=response.data                 
                  var geo_array=GeoData[data.project_name][data.network_name][data.type];
                  if(!geo_array.includes(data.label_name))
                      GeoData[data.project_name][data.network_name][data.type].push(data.label_name)
                  logic.conditions = {};
                  logic.input_name = null;
                  logic.label_modal = false;  
              }, function myError(response) {
                  server.parseError(response);
              });


      }



    
    this.checkGeometryHealthEx=function(geometry,network_name){
      cookie_name=this.getCookie("csrftoken");
      var server=this;
      data=General.getNetworkInfo();      
      $http({
                method : "POST",
                url : "/api/health/"+General.project_name+"/"+geometry.pores+"/"+geometry.throats,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:data
            }).then(function mySuccess(response) {
                data=response.data;

                if(!data["present"]){
                  $("#error-response").text("Pores Collision")
                  $(".error-message").show().delay(3000).fadeOut();                  
                }                     
                else{
                  name=General.project_name;                     
                  ProjectsData[name][network_name]["geometries"][geometry.name]=geometry.inputs_data;
                  geometry.getBuiltInModels();
                  geometry.attachPoresThroats(name);
                  console.log("sss",General.getNetworkInfo())
                  server.saveToDB(General.project_name,General.getNetworkInfo());

                  // geometry.checkPores(name,geometry.pores)
                  // geometry.checkThroats(name,geometry.throats)
                }                              


            }, function myError(response) {
               server.parseError(response);
            });
    }

    this.getMethods=function(type){
      var server =this;
      cookie_name=this.getCookie("csrftoken");
      
      $http({
                method : "POST",
                url : "/api/methods/"+type,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                
            }).then(function mySuccess(response) {
                if(type.toLowerCase() == "algorithms"){
                  AlgorithmsData["data"]=response.data; 
                }                              


            }, function myError(response) {
               server.parseError(response);
            });
    }

    this.runAlgorithm=function(project_name,network_data){
      var server =this;
      cookie_name=this.getCookie("csrftoken");
      
      $http({
                method : "POST",
                url : "/api/run/"+project_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
                
            }).then(function mySuccess(response) {
                var data=response["data"];
                var url=[project_name,data.hash].join("/");
                window.open(url);                                      


            }, function myError(response) {
              
               server.parseError(response);
            });
    }
    this.downloadFile=function(scope,project_name){
      var server = this;
      network=General.getNetworkInfo();
      cookie_name=this.getCookie("csrftoken");
      
      $http({
                method : "POST",
                url : "/api/generate/"+project_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
                
            }).then(function mySuccess(response) {
                var data=response.data;
                if(data.project_name){                  
                  scope.hdf5_flag=true;                                                      
                  server.download(project_name,"hdf",data.hash)
                  server.download(project_name,"xmf",data.hash)
                  server.download(project_name,"pnm",data.hash)
                
                }
                  
                //window.location = url;
                                                      


            }, function myError(response) {
                server.parseError(response);
            });
    }

    this.remove=function(project_name){
      cookie_name=this.getCookie("csrftoken");
      $http({
            method : "POST",
            url : "/api/remove/"+project_name,
            headers:{
              "Content-Type":"application/text",
              "X-CSRFToken":cookie_name
            },
            
            
        }).then(function mySuccess(response) {
            var data=response["data"];
            window.location="/web/dashboard/";                                      


        }, function myError(response) {
           console.log("error",response);
        });
    }

   

    this.download=function(project_name,type,hash){
      var url=[window.host,"api","file",project_name,type,hash].join("/");      
      $("#"+type).attr('href',url);
       
     
      //document.body.appendChild(anchor);
      //anchor.click();
    }
    

    this.getNetworkList=function(){
      cookie_name=this.getCookie("csrftoken");
    }
    
    this.parseError=function(response){
      var html = $.parseHTML(response.data);
      var message=$(html).find(".exception_value").text();                 
      var ajax_response = $(".ajax_response");
      var heading = $(".ajax_response > .heading");
      //var message = $(".ajax_response > .message");
      //ajax_response.fadeIn();
      //ajax_response.addClass("ajx_request");
      //heading.text("Error message");
      //message.text(message);
      alertify.reset()
                  .maxLogItems(2)                  
                  .delay(5000)
                  .logPosition("top right")
                  .error(message);
      //setTimeout(function(){ $('.ajax_response').fadeOut() }, 3000);
     return message;
      
    }
    this.messageError=function(error_type,error_message){
      alertify.reset()
                  .maxLogItems(2)                  
                  .delay(2000)
                  .logPosition("top right")
                  .error(error_message);
    }
    this.messageAlert=function(message){
      alertify.reset()
                  .maxLogItems(2)                  
                  .delay(2000)
                  .logPosition("top right")
                  .log(message);
    }
    this.downloadScript=function(project_name){
      var server = this;
      var network_data=General.getNetworkInfo();
      cookie_name=this.getCookie("csrftoken");
      $http({
            method : "POST",
            url : "/api/script/"+project_name,
            headers:{
              "Content-Type":"application/text",
              "X-CSRFToken":cookie_name
            },
            data:network_data
            
            
        }).then(function mySuccess(response) {
           var data=response.data
           var url=[window.host,"api","python",project_name,data.hash].join("/");
           var anchor = document.createElement('a');
           anchor.href = url;
           document.body.append(anchor);
           anchor.click();
                                              


        }, function myError(response) {
           console.log("error",response);
        });
    }

    this.settings=function(data,setting=false){
      var server = this;
      var params = {"settings":setting};      
      cookie_name=this.getCookie("csrftoken");
      $http({
            method : "POST",
            url : "/web/settings/",
            params:params,           
            headers:{
              "Content-Type":"application/json",
              "X-CSRFToken":cookie_name
            },            
            data:data
            
            
        }).then(function mySuccess(response) {
            if(!setting){
              SettingsData["data"]=response.data;
              
            }else{
              window.location="/web/dashboard";
            }

        }, function myError(response) {
           console.log("error",response);
        });
    }




}).service("General",function(ProjectsData){
  this.project_name=null;


  this.getNetworkInfo=function(){
    var general=this;
    var value=ProjectsData[general.project_name];   
    for(x in value){
     return value[x];
    }
    return;
  }

  this.objAlreadyExist=function(name){
    var general = this;
    var value =ProjectsData[general.project_name];   
    for(key in value["geometries"]){
      if(key == name) return true;
    }
    for(key in value["phases"]){
      if(key == name) return true;
    }
    for(key in value["physics"]){
      if(key == name) return true;
    }
    return false;

  }
  
  
});

