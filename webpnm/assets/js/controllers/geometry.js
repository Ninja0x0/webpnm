
angular.module("DashApp").controller("GeometryController",['$http','GeoData','ServerCalls','General',function($http,GeoData,ServerCalls,General){
  var geometry=this;
  geometry.header_string="Create Geometry";
  geometry.inputs={};
  geometry.pores=null;
  geometry.throats=null;
  geometry.type={};
  geometry.name=null;  
  geometry.types={};
  geometry.mode={};
  geometry.list_modes={"union":{},"not":{}}
  geometry.inputs_data=null; //all the data of input fields while creating new geometry


  geometry.init=function(flag){
    console.log(ServerCalls);
    ServerCalls.getTypeData('Geometry');
    geometry.pores=null;
    geometry.throats=null;    

  }
  

  geometry.clear=function(){
    geometry.pores=null;
    geometry.throats=null;
    geometry.name=null;
    geometry.type=null;
    geometry.inputs_data=null; //all the data of input fields while creating new geometry
  }

  geometry.add=function(){
    
    network_data=General.getNetworkInfo();
      if(network_data!=undefined){ 
        if(General.validation(geometry.name)){
              throat_label=geometry.throats      
          value={name:geometry.name,type:geometry.type,
                 pores:geometry.pores,throats:throat_label,
                 inputs:geometry.inputs,physics:{},models:{},built_models:{},built_change:{},mode:geometry.mode};
          
          geometry.inputs_data=value;       
          ServerCalls.checkGeometryHealthEx(geometry,network_data.name)
        }else{
          ServerCalls.messageError("Error","Name should not contain special characters and space.")
        }      
        

    }        
  }

  geometry.getTypes=function(){  
       geometry.types=ServerCalls.types['Geometry'];   
       return ServerCalls.types['Geometry'];
  }
  geometry.getInputs=function(type_name){
    if(type_name =="GenericGeometry"){        
        delete geometry.types[type_name]["pores"]
        delete geometry.types[type_name]["throats"]
        delete geometry.types[type_name]["network"]
    }
    geometry.inputs=geometry.types[type_name];
    return geometry.types[type_name];
  }

  geometry.getBuiltInModels=function(){
      ServerCalls.getBuiltInModels('Geometry',geometry.inputs_data);
  }
  geometry.getPores=function(project_name){   
    network_data=General.getNetworkInfo()  
    if(network_data != undefined)
        if(GeoData[project_name])
          if(GeoData[project_name][network_data.name] != undefined)
            return GeoData[project_name][network_data.name]["pores"];
    else return;
  }
  geometry.getThroats=function(project_name){     
     network_data=General.getNetworkInfo()   
     if(network_data != undefined)
       if(GeoData[project_name])
        if(GeoData[project_name][network_data.name] != undefined){          
          return GeoData[project_name][network_data.name]["throats"];
        }
          
    else return;
  }
  
  geometry.checkPores=function(project_name,pore){
    network_data=General.getNetworkInfo()
    pore_data=GeoData[project_name][network_data.name]["pore_health"]
    if(pore=="pore.all"){
      pore_data=[]
    }else{
    for(index in pore_data){
      if(pore_data[index]==pore){
         pore_data.splice(pore_data.indexOf(pore),1)
         pore_data.splice(pore_data.indexOf("pore.all"),1)
         break;
      }
    }
  }
  
    GeoData[project_name][network_name]["pore_health"]=pore_data
      
  }

  geometry.checkThroats=function(throats){
    network_data=General.getNetworkInfo()
    throat_data=GeoData[project_name][network_data.name]["throats_health"]
    if(throats=="throats.all"){
     
    }else{
    
    } 
    
  }


 // function tell geometry attached pores and throats
  geometry.attachPoresThroats=function(project_name){
      network_name=General.getNetworkInfo().name
      GeoData[project_name][network_data.name]["attached_pores"][geometry.pores]={}
      GeoData[project_name][network_data.name]["attached_throats"][geometry.throats]={}
      
  }

  // Refactor this code and make it one common function
  geometry.help=function(module_name,class_name){
      ServerCalls.getHelpString(module_name,class_name);
  }


}]);
