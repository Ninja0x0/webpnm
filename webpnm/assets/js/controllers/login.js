
angular.module('LoginApp').controller("LoginController",['$scope','$http',function($scope,$http){
	
    $scope.loading=false;

	$scope.submit=function(type){

    		if(type=="create"){
    			var credentials={username:$scope.username,
    							email:$scope.email,
    							password:$scope.password};

    		}else if(type=="login"){
    			var credentials={username:$scope.login_name,
    							password:$scope.login_pass};
    		}		
     
       
		
		cookie_name=$scope.getCookie("csrftoken");
		$http({
                method : "POST",
                url : "/web/submit/",
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                params:credentials,
                data:{"type":type}
            }).then(function mySuccess(response) {                
                data=response.data
                console.log(data)
                if(data["message"]=="ok"){
                	window.location ="/web/dashboard/"
                }
                else{
                    $scope.errorMessage(type,data);
                }


            }, function myError(response) {
               console.log("error",response);
            });
	}
    $scope.errorMessage=function(type,response){        
        var error_message=$(".error_message");
        error_message.fadeIn();
        error_message.css('display','block');
        $(".error_message").text(response["message"]);
        setTimeout(function(){ $('.error_message').fadeOut() }, 3000);
       
        
    }
	
   

	$scope.getCookie=function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }
}]);