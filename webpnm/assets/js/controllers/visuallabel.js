angular.module("DashApp").controller("LabelController",['$scope','ServerCalls','ProjectsData','LabelData','General',function($scope,ServerCalls,ProjectsData,LabelData,General){
  var label=this;
  label.viewdata=false;
  label.project_name=null;
  label.network_name=null;
  label.label_name=null;


  label.show=false;
  
  label.init=function(project_name){
    network=General.getNetworkInfo();
    label.project_name=project_name;
    label.network_name=network.name;
    label.show=true;    
     
  }
  label.addLabel=function(type){
    $scope.addLabel(type);
  }
  label.applyLabels=function(project_name){   
    ServerCalls.applyLabels(project_name);         
  }
  label.update=function(data){
    LabelData["data"]=data;
  }
  label.getCube=function(){
    return ProjectsData[label.project_name][label.network_name]["inputs"]["shape"];
  }
  label.getCustomLabels=function(){    
    return ProjectsData[label.project_name][label.network_name]["labels"];
  }
  label.removeCustomLabel=function(label_name){    ;
    delete ProjectsData[label.project_name][label.network_name]["labels"][label_name];
  }
  label.getCustomLabel=function(label_name){    
    return ProjectsData[label.project_name][label.network_name]["labels"][label_name];
  }
  label.getMesh=function(label_name){    
    return ProjectsData[label.project_name][label.network_name]["labels"];   [label_name];    
  }
  
}]);