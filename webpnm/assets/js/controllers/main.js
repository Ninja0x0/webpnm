angular.module("DashApp").controller("MainController",['$scope','$http','ProjectsData','GeoData','ServerCalls','General','ModelsData',function($scope,$http,ProjectsData,GeoData,ServerCalls,General,ModelsData){
  var main=this;
  main.network_name=null;
  main.network_type=null;
  main.networks_data=null;
  main.geometry_name=null;
  main.phase_name=null;


  main.init=function(){
    main.networks_data=General.getNetworkInfo()
    console.log(main.networks_data)
    

  }

  

  main.createNetwork=function(network){    
    General.project_name=network.open_project;   
    if(General.validation(network.network_name)){
      network.network_flag=false;
      ServerCalls.createNetwork(main,network);

    }else{
      ServerCalls.messageError("Error","Name should not contain special characters and space.")
    }
       
  }

  main.update=function(geometry_name,phase_name){      
      main.geometry_name=geometry_name;
      main.phase_name=phase_name;
  }

  main.getGeometry=function(){
      var networks_data=General.getNetworkInfo();          
      if(network_data != undefined) 
        return network_data["geometries"];
      else return;
  }



  main.removeGeometry=function(project_name,geometry_name){
     var network=General.getNetworkInfo();     
     delete ProjectsData[project_name][network.name]["geometries"][geometry_name];
     ServerCalls.saveToDB(project_name,network);
  }

  main.getPhases=function(){
      network_data=General.getNetworkInfo()    
     if(network_data != undefined) 
        return network_data["phases"];
      return;
  }
  main.removePhase=function(project_name,phase_name){
     var network=General.getNetworkInfo();     
     delete ProjectsData[project_name][network.name]["phases"][phase_name];
     ServerCalls.saveToDB(project_name,network);
  }
  // main.addPhysicsBoth=function(network_name,geometry_name,phase_name){
      
  //      attach=network.physicsinputs[network_name]['attach'];
  //      value={name:network.physicsinputs[network_name]["name"],geometry:geometry_name,
  //      type:network.physicsinputs[network_name]["type"],phase:phase_name};
  //      network.lists[network_name]["physics"][network.physicsinputs[network_name]["name"]]=value;
  //      console.log(network.lists[network_name]);
  // }
  main.getPhysicsBoth=function(geometry_name,phase_name){
      var network=General.getNetworkInfo();      
      var values=network["physics"];
      for(name in values){
          if(values[name]["geometry"]==geometry_name)
                if(values[name]["phase"]==phase_name)
                  return values[name]["name"];
      }      
      return;

  }
  main.getPhysicsData=function(geometry_name,phase_name){
      var network=General.getNetworkInfo();   
      var values=network["physics"];      
      for(name in values){
          if(values[name]["geometry"]==geometry_name)
                if(values[name]["phase"]==phase_name)
                  return values[name];
      }
      return;
  }
  main.removePhysicsBoth=function(project_name,geometry_name,phase_name){
      var network=General.getNetworkInfo();   
      var values=network["physics"];   
      console.log(values);
      for(name in values){
       
        if(values[name]["geometry"]==geometry_name)
                if(values[name]["phase"]==phase_name){
                    delete network["physics"][name];
                }                    
      }
      ServerCalls.saveToDB(project_name,network);
  }
  
  main.viewModels=function(type,name,type_name){
    ModelsData["type"]=type;
    ModelsData["name"]=name;
    network=General.getNetworkInfo();    
    ServerCalls.getModels(type,main.network_name,name);
    if(type.toLowerCase() == "geometry"){
      type_data=network.geometries[name];
    }else if(type.toLowerCase() == "phases"){
      type_data=network.phases[name];
    }else if(type.toLowerCase() == "physics"){
      type_data=network.physics[name];
    }
    ServerCalls.getBuiltInModels(type,type_data);

  }

  main.showModalType=function(scope){
    console.log(scope);
  }
  main.createName=function(scope,type){
    while(true){
      var name=type+"_"+Math.floor(Math.random()*100);
      if(!General.objAlreadyExist(name)){
        scope.name=name;
        break;
      }
    }
    
  }

}]);