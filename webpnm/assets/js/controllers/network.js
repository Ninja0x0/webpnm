angular.module("DashApp").controller("NetworkController",['$http','ProjectsData','NetworkData','ModelsData','GeoData','GraphData','LabelGraphData','ServerCalls','General','SettingsData',function($http,ProjectsData,NetworkData,ModelsData,GeoData,GraphData,LabelGraphData,ServerCalls,General,SettingsData){
  var network=this;
  var CHOOSE_TYPE="Choose type ..."
  network.blacklist_networks=["genericnetwork","cubictemplate"];
  network.blacklist_inputs=["project","name"];
  network.project_name=null;
  network.network_name=null;
  network.open_project=null;
  network.lists={};
  network.network={};
  network.geometry={};
  network.phases={};
  network.fetchtypes={}
  network.networkinputs={}
  network.geoinputs={};
  network.phaseinputs={};
  network.physicsinputs={};
  network.prop={}
  network.geophy={}
  network.attached=[];
  network.algorithim={};
  network.graphdata={};
  network.fetchdata={};
  network.ajax={}
  network.settings_inputs={};

  //--flags---
  network.main_page=true;
  network.spinner=true;
  network.showModels=false;
  network.grid_page=true;
  network.label_page=false;
  network.viz_page=false;
  network.simulation_page=false;
  network.create_flag=true;
  network.network_flag=false;
  network.project_create=false;
  network.project_view=false;
  network.hide_table=true;
  network.settings_flag=false;
  network.hdf5_flag=false;
//---------------------//

//----class variable ---//
  
//------------------------//

//---------Errors----------//
  network.errorClass="";
  network.error="";

  

//------------------------//




  network.fetchdata=NetworkData
  
  network.select_type=CHOOSE_TYPE
  network.inputs={};


network.settings=function(){
  network.settings_flag=true;
  network.project_view=false;
  network.main_page=false;
}

network.getSettings=function(local=false){
  if(local){
     network.settings_inputs=SettingsData["data"]["data"];
  }else{
    ServerCalls.settings({})
  }
}

network.applySettings=function(){  
  ServerCalls.settings(network.settings_inputs,true);
}


network.dashboard=function(){
  network.settings_flag=false;
  network.project_view=false;
  network.main_page=true;
}
//------------------------------//
network.blacklist=function(param,blacklist){
   
  if(blacklist.includes(param)) return true;
  else false;
}

//---- project functions --- //
 

  network.createProject=function(){
    if(network.validateProject()){
      ProjectsData[network.project_name]={};
      network.viewProject(network.project_name);
      setTimeout(function(){ 
        var children = $("#project_list").children();
        console.log(children.length)
        for(x=0;x<children.length;x++){
          var data_name = children[x].getAttribute("data-name")                  
          if(data_name=network.project_name){
            network.selectClass(children[x]);
          }
        } 
      }, 100);
      
      //
    }
  }

  network.validateProject=function(){
    for(key in ProjectsData){
       if(key == network.project_name){
          network.errorClass="show";
          network.error="Project name already exists"
          return false;
       }else if(!General.validation(network.project_name)){
          network.errorClass="show";
          network.error="Name should not contain any special characters and space";
          return false;
       }
    }
    if(!network.project_name){
          network.errorClass="show";
          network.error="Project name is empty";
          return false
    }      
    network.errorClass="";
    network.error="";
    return true;
  }

  network.viewProjectWindow=function(bool_type){
    network.project_view=bool_type;
    network.main_page=false;
    network.settings_flag=false;
  }
  
  network.viewProject=function(project_name){
    network.open_project=project_name;
    General.project_name=project_name;    
    var value=ProjectsData[project_name];
    if(!network.isEmpty(value)) {   
        
    }
    ModelsData.clear();
    network.clearInputs();
    network.containsNetwork();
    network.viewProjectWindow(true);
    network.initFlag();
    network.network_flag=false;
    network.deselectHeader();
    return;
  }
  
  network.deselectHeader = function(){    
    $(".common-header>.selected").removeClass("selected");    
    $(".common-header>a").first().addClass("selected");
  }

  network.getProjectList=function(){    
    return ProjectsData;
  }

  network.containsNetwork=function(){
    network.hide_table=network.isEmpty(ProjectsData[General.project_name]);   
  }

  network.insertNetworkIntoProject=function(project_name,network_data){
    ProjectsData[project_name]=network_data;
    console.log(ProjectsData);
  }

  network.initFlag=function(){
    network.grid_page=true;
    network.label_page=false;
    network.viz_page=false;
    network.simulation_page=false;
  }
//------------------------------------//

  network.getNetworkInfo=function(){
    data=General.getNetworkInfo();    
    return General.getNetworkInfo();
  }


  //clear all the inputs
  network.clearInputs=function(){
    network.network_name=null;
    network.inputs={};
    network.select_type=CHOOSE_TYPE;
  }
  


  network.showNetwork=function(network_name){
    network.network_flag=true;
    network.clearInputs();
    network.name=network_name;
    console.log(ResultData)
    network.type=ResultData["data"][network_name]["type"];   
    console.log(network_name);
  }



  network.getNetwork=function(){    
    ServerCalls.fetchTypes('Network');
    network.network_name=["net_",Math.floor(Math.random()*10)].join("");
  }

  network.getNetworkData=function(){
    
    return NetworkData["data"];
  }

  network.getNetworkInputs=function(type){

    return NetworkData["data"][type];
  }

  network.applyNetwork=function(){
     network.lists[network.network_name]={name:network.network_name,
                                  type:network.select_type,
                                  inputs:network.inputs};
     network.ajax[network.network_name]={};
     network.lists[network.network_name]["geometries"]={};
     network.lists[network.network_name]["phases"]={};
     network.lists[network.network_name]["physics"]={};
     network.lists[network.network_name]["labels"]={};
     
     return network.lists[network.network_name]
     
  }



//-----------------------------------//

  network.saveJson=function(){
    ResultData["data"]=network.lists;
    console.log(ResultData);
  }

  network.parseProject=function(json_file){    
    console.log(json_file)
    for(i in json_file){
        list={}
        var project=json_file[i];        
        network_name=project.network_name;
        project_name=project.project_name;
        list[network_name]=project["data"];

        ProjectsData[project_name]=list;
        GeoData[project_name]={}
        GeoData[project_name][network_name]=project["data"]["geo_data"];
        if(project["data"]["graph_data"] != undefined){
          GraphData["data"][project_name]={};
          GraphData["data"][project_name][network_name]=project["data"]["graph_data"]
        }
         if(project["data"]["logic_data"] != undefined){
            LabelGraphData["data"][project_name]={};
            LabelGraphData["data"][project_name][network_name]=project["data"]["logic_data"]
         }
    }
    
  }

  network.addNetwork=function(){

    network.lists[network.inputnetwork]={width:"",height:"",spacing:"",con:"",num_points:"",
                                    geometries:{},physics:{},phases:{},name:network.inputnetwork,labels:{}};
    network.saveJson();                          
   
  }
  network.setNetwork=function(network_name){ 
    console.log(".............",network_name)
    console.log("...ss.....",network.lists)
    network.fetchdata={"data":network.lists[network_name]};
    network.networkinputs={}
    network.fetchTypes('Network');
    label_page=false;
    network.viz_page=false;
    grid_page=true;

  }
  network.viewNetwork=function(){ 
    console.log(network.fetchdata)  
    return network.fetchdata;    
  }
  network.viewTypes=function(type){    
    return network.fetchtypes[type];    
  }
  network.viewInputTypes=function(type,name,prop){      
      if( prop == ''  || prop === undefined) return '';      
      else{        
        network.network[name]=network.fetchtypes[type][prop];
        return network.fetchtypes[type][prop];

      }

  }
  network.viewGeometryDivision=function(network_name){
     return network.geoinputs[network_name]["data"];
  }

  network.attachNetwork=function(network_name){
    network_to=angular.element("."+network_name+network_name).text();
    network.attached.push([network_name,network_to]);

  }
 

  network.getNetworks=function(){
        return ResultData["data"];
  }
   
  network.isEmpty=function(obj){
      for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
      }
      return true;
  }

 
  

  
  


 

  network.viewModels=function(type,network_name,type_name){
    ServerCalls.getModels(type,network_name,type_name);
    console.log("ssaw",ModelsData);
    
  }

  network.openViz=function(network_name){
    window.open("/web/results/"+network_name+"/","_blank");
  }

  network.selectClass=function(element){
      $('.selected_item').removeClass('selected_item');
      $(element).addClass('selected_item');
  }

  network.labelPage=function(){
    network.label_page=true;
    network.grid_page=false;
    network.viz_page=false;
    network.simulation_page=false;
  }
  network.gridPage=function(){
    network.label_page=false;
    network.grid_page=true;
    network.viz_page=false;
    network.simulation_page=false;
  }
  network.vizPage=function(){
    network.label_page=false;
    network.grid_page=false;
    network.viz_page=true;
    network.simulation_page=false;
  }

  network.simulationPage=function(){
    network.label_page=false;
    network.grid_page=false;
    network.viz_page=false;
    network.simulation_page=true;
  }
  



  network.logout=function(){
    cookie_name=network.getCookie("csrftoken");
    $http({
                method : "POST",
                url : "/web/logout",
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
                if(response.data["message"]=="ok")
                  window.location="/web/login"         
                
                   

            }, function myError(response) {
               console.log("error",response);
            });
  }
  network.createModels=function(){    
    var formData = new FormData(document.getElementById("uploadModels"));    
    cookie_name=network.getCookie("csrftoken");
    $.ajax({
          url: '/web/newmodel',
          type:'POST',
          data: formData,
          cache: false,
          processData: false,
          contentType: false,
          success: function(data) {              
          }
    });


  }
  network.fetchTypes=function(type){

    
    cookie_name=network.getCookie("csrftoken");

    $http({
                method : "POST",
                url : "/web/types/"+type,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
                network.fetchtypes[type]=response.data;            
                   

            }, function myError(response) {
               console.log("error",response);
            });


  }
  network.loadNetwork=function(){
    network.getSettings();
    ServerCalls.loadNetworkDB(network);
  }
  
  

  network.removeNetwork=function(project_name){
    ServerCalls.remove(project_name);
  }

  network.getFile=function(network_name,file_ext){
      
      cookie_name=network.getCookie("csrftoken");
      $http({
                method : "POST",
                url : "/web/generate/"+file_ext,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:network.lists[network_name]
            }).then(function mySuccess(response) {
                var json_data=response.data;
                var type=json_data["type"]
                var url="/web/file/"+type+"/"+Credentials["username"]+"/"+json_data["network"]+"/"+json_data["id"]
                console.log(url)                                       
                window.location=url;
            }, function myError(response) {
               console.log("error",response);
            }); 
      
  }
 
  network.getCookie=function(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
  

  network.getNetworkList=function(network_name){   //function not works on internet explorer except v11
      var arr=network.attached;
      var temp=[];
      for(i=0;i<arr.length;i++){
          set=new Set([arr[i][0],arr[i][1]]);
          if(set.has(network_name)){
              set.delete(network_name);
              for(let x of set) temp.push(x);
          }
      }
      console.log("temp",temp);
      return temp;

  }

network.removeAttachedNetwork=function(network_name,network_to){
      var arr=network.attached; //checikingg.......
      for(i=0;i<arr.length;i++){
          set=new Set([arr[i][0],arr[i][1]]);
          if(set.has(network_name)){
             if(set.has(network_to)){
                network.attached.splice(i,1);
             }
          }
      }
}


network.download=function(project_name){
  ServerCalls.downloadFile(network,project_name);
}

network.downloadScript=function(project_name){
  ServerCalls.downloadScript(project_name);
}

}]);