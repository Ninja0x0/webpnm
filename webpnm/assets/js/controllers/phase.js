angular.module("DashApp").controller("PhasesController",['$http','ServerCalls','General',function($http,ServerCalls,General){
  var phases=this;
  phases.inputs={};
  phases.type=null;
  phases.types=null;
  phases.name=null; 

  phases.init=function(){
    ServerCalls.getTypeData('Phases');
  }

  phases.add=function(project_name){
    var network=General.getNetworkInfo(project_name);
    if(General.validation(phases.name)){
        value={name:phases.name,type:phases.type,models:{},physics:{},inputs:phases.inputs,built_models:{},built_change:{}};
        phases.getBuiltInModels(project_name,value)    
        network["phases"][phases.name]=value;
        ServerCalls.saveToDB(project_name,General.getNetworkInfo())
    }else{
      ServerCalls.messageError("Error","Name should not contain special characters and space.")
    }
    
  }
  phases.getTypes=function(){
    phases.types=ServerCalls.types['Phases'];
    
    return ServerCalls.types['Phases'];   
  }
  phases.getBuiltInModels=function(project_name,phases_data){
      ServerCalls.getBuiltInModels('Phases',phases_data);
  }
  phases.help=function(module_name,class_name){
      ServerCalls.getHelpString(module_name,class_name);
  }
}]);