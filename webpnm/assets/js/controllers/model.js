angular.module("DashApp").controller("ModelController",['$http','ServerCalls','ModelsData','ClipBoard','ProjectsData','General','ModelsData',function($http,ServerCalls,ModelsData,ClipBoard,ProjectsData,General){
  var model=this;  
  model.list={};  
  model.name="";
  model.method="";
  model.inputs={};
  model.propname="";
  model.info=ModelsData["info"];
  model.type={};
  model.data=ModelsData["data"];
  model.prefix="pore";
  model.attached_models=null;
  model.project_name=null;
  model.disabled = false;

  model.removeModelBox=function(){
    model.data={};
    model.name=null;
    model.method=null;
    model.inputs={};
    model.propname=null;
    model.disabled = false;
  }

  
  model.blacklist=function(param){
    var blacklisted=["target"];
    if(blacklisted.includes(param)) return true;
  }

  model.isEmpty=function() {    
    for(var key in model.data) {
        if(model.data.hasOwnProperty(key)){            
            return true;
        }            
    }
    return false;
  }
  model.getInputData=function(){
    var name=[model.prefix,model.propname].join("_")    
    var json_data={};
    json_data["model_name"]=model.name;
    json_data["model_method"]=model.method;
    json_data["model_inputs"]=model.inputs;   
    var data={[json_data.model_name]:{[json_data.model_method]:json_data.model_inputs}};    
    return {"name":name,"data":data};
  }


  model.getBuiltModels = function(project_name){
    model.project_name =project_name;
    var network=General.getNetworkInfo();
    var type = ModelsData["type"]
    var name = ModelsData["name"]    
    if(network && project_name){      
      var data=ProjectsData[project_name][network.name]
      if(type == "geometry"){
        model.getChangeBuiltIns(data["geometries"][name]);
        return data["geometries"][name]["built_models"];
      }else if(type == "phases"){
        model.getChangeBuiltIns(data["phases"][name]);
        return data["phases"][name]["built_models"]
      }else if(type == "physics"){
        model.getChangeBuiltIns(data["physics"][name]);
        return data["physics"][name]["built_models"]
      }
    }else{
      console.log("Either project_name or network_name not defined");
    }
   return;
    
  }
  model.getChangeBuiltIns=function(data){
    for(key in data["built_change"]){
      if(key in data["built_models"]){
          data["built_models"][key] = {};
          data["built_models"][key] = Object.assign(data["built_change"][key]);
      }
    }
  }


  model.getAttachedModels = function(project_name){
    var network=General.getNetworkInfo();
    var type = ModelsData["type"]
    var name = ModelsData["name"]
    if(network && project_name){
      var data=ProjectsData[project_name][network.name]     
      if(type == "geometry"){
        return  data["geometries"][name]["models"];
      }else if(type == "phase"){
        return data["phases"][name]["models"]
      }else if(type == "physics"){
        return data["physics"][name]["models"]
      }
    }
    return;
  }

  model.attachModel=function(project_name){
    var type_data = null;
    if(model.propname!=""){
        var network=General.getNetworkInfo();
        if(General.validation(model.propname)){
              var type = ModelsData["type"];
              var name = ModelsData["name"];
              var data = ProjectsData[project_name][network.name];                 
              var model_data=Object.assign({},model.getInputData());                    
              model.project_name=project_name;
              if(type == "geometry"){

                type_data=data["geometries"][name]                
                if(model_data.name in type_data["built_models"]){                       
                  data["geometries"][name]["built_change"][model_data.name]=model_data.data;
                }else{
                  data["geometries"][name]["models"][model_data.name]=model_data.data;
                }     
               
                  
              }else if(type == "phase"){
               
                type_data=data["phases"][name]                
                if(model_data.name in type_data["built_models"]){                       
                  data["phases"][name]["built_change"][model_data.name]=model_data.data;
                }else{
                  data["phases"][name]["models"][model_data.name]=model_data.data;
                }  

              }else if(type == "physics"){
                
                type_data=data["physics"][name]                
                if(model_data.name in type_data["built_models"]){                       
                  data["physics"][name]["built_change"][model_data.name]=model_data.data;
                }else{
                  data["physics"][name]["models"][model_data.name]=model_data.data;
                }  
              }              
              model.removeModelBox();
        }else{
          ServerCalls.messageError("Error","Name should not contain special characters and space.")        
        }
        
      }else{
        ServerCalls.messageAlert("Empty model name");
      }
      
    
  }
 
    

  
  model.removeModelBuiltIn=function(project_name,model_name){
    var network=General.getNetworkInfo();
    var type = ModelsData["type"]
    var name = ModelsData["name"]
    var data=ProjectsData[project_name][network.name]
    if(type=="geometry")
         model.findAndDelete(data["geometries"][name]["built_models"],model_name);
    if(type=="phase")
         model.findAndDelete(data["phases"][name]["built_models"],model_name);
    if(type=="physics")
         model.findAndDelete(data["physics"][name]["built_models"],model_name);
    
  }
  model.findAndDelete=function(data,model_name){
    for(key in data){
      if(data[key] == model_name){
        delete data[key]
        return
      }
    }
  }
  model.removeModel=function(project_name,model_name){
    var network=General.getNetworkInfo();
    var type = ModelsData["type"]
    var name = ModelsData["name"]
    var data=ProjectsData[project_name][network.name]
    if(type=="geometry")
        delete data["geometries"][name]["models"][model_name];
    if(type=="phase")
        delete data["phases"][name]["models"][model_name];
    if(type=="physics")
        delete data["physics"][name]["models"][model_name];
    
  }
 
 

  model.getModelNames=function(){
      model.data=ModelsData["data"];
      
      var arr=[]
       for(key in model.data){            
            if(key.split("_").includes(model.prefix)){

              arr.push(key);
            }
            if(!key.split("_").includes("pore") && !key.split("_").includes("throat") ) {
              arr.push(key);
            }        
            
       }

      return arr;      
  }

  model.getModelMethods=function(model_name){
    var arr=[]
    if (model_name  != null){
      for(key in model.data[model_name])
        arr.push(key)
    }    
    return arr;
  }

  model.getModelInputs=function(model_name,method_name){
    var obj={};
    var built_models=model.getBuiltModels(model.project_name);
    var attached_models = model.getAttachedModels(model.project_name);
    var name = [model.prefix,model.propname].join("_");
    if (model_name  != null && model_name !=""){
        
        if(name in built_models){
          if(model_name in built_models[name]){
            if(method_name in built_models[name][model_name]){
              model.disabled = true;
              return built_models[name][model_name][method_name]
            }      
          }          
        }else if(name in attached_models){
          console.log(attached_models)
          if(model_name in attached_models[name]){
            if(method_name in attached_models[name][model_name]){
              model.disabled = true;
              return attached_models[name][model_name][method_name]
            }      
          } 
        }
        list=model.data[model_name][method_name]
        for(key in list){         
          if(key=="geometry" || key=="network" || key=='phase' || key=='physics');
          else {                  
            obj[key]=list[key];            
          }
        }          
    }     //setting the default values
   
    return obj;
  }  
 

  model.initInputs=function(project_name,label_name){
    var network=General.getNetworkInfo();
    var type = ModelsData["type"]
    var name = ModelsData["name"]
    var data=ProjectsData[project_name][network.name]
    model.propname=label_name.split("_")[1];
    model.prefix=label_name.split("_")[0];

    console.log(ModelsData)
    if(type=="geometry"){
        model.name=Object.keys(data["geometries"][name]["models"][label_name])[0];
        model.method=Object.keys(data["geometries"][name]["models"][label_name][model.name])[0];         
        model.inputs=Object.assign({},data["geometries"][name]["models"][label_name][model.name][model.method]);
    }else if(type=="phase"){         
        model.name=Object.keys(data["phases"][name]["models"][label_name])[0];
        model.method=Object.keys(data["phases"][name]["models"][label_name][model.name])[0];         
        model.inputs=Object.assign({},data["phases"][name]["models"][label_name][model.name][model.method]);
    }else if(type=="physics"){    
        model.name=Object.keys(data["physics"][name]["models"][label_name])[0];
        model.method=Object.keys(data["physics"][name]["models"][label_name][model.name])[0];                 
        model.inputs=Object.assign({},data["physics"][name]["models"][label_name][model.name][model.method]);
    }

  }

  model.populateModels=function(project_name,label_name){
      
      var network=General.getNetworkInfo();
      var type = ModelsData["type"]
      var name = ModelsData["name"]
      var data=ProjectsData[project_name][network.name]
      
      if(type=="geometry"){
        console.log(data);
        var label_data=data["geometries"][name]["built_models"][label_name];
        model.parseModels(label_name,label_data)
      }else if(type=="phases"){         
        var label_data=data["phases"][name]["built_models"][label_name];
        model.parseModels(label_name,label_data)
         
      }else if(type=="physics"){    
         var label_data=data["physics"][name]["built_models"][label_name];
         model.parseModels(label_name,label_data)
      }
  }

  model.parseModels=function(label_name,label_data){
      var a = label_name.split("_");
      a.shift();      
      val = a.join("_");
      model.propname=val;
      model.prefix=label_name.split("_")[0];    
      model.name=Object.keys(label_data)[0];
      model.method=Object.keys(label_data[model.name])[0];
      model.inputs=Object.assign({},label_data[model.name][model.method]);
      model.disabled=true;
  }

 
}]);
