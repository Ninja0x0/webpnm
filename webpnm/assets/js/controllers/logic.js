angular.module("DashApp").controller("LabelLogicController",['$scope','ServerCalls','GeoData','General','LabelGraphData',function($scope,ServerCalls,GeoData,General,LabelGraphData){
  var logic=this;
  logic.logiclist=["union","intersection","difference","not"];
  logic.throatmodes=["union","intersection","difference"];
  logic.queries=["neighbor_pores","common_pores","unique_pores","neighbor_throats","common_throats","unique_throats"]
  logic.storage={};
  logic.stack_storage={};
  logic.flag_name="pores";
  logic.input_name="";
  logic.network_name=null;
  logic.project_name=null;
  logic.type=null;
  logic.type_flag=null;
  logic.label_modal = false;
  
  logic.global_count = 0;
  // variables for logic operations
  logic.conditions = {};
  


  logic.init=function(project_name){
     network_data=General.getNetworkInfo();
     logic.project_name=project_name;
     logic.network_name=network_data.name;
     
  }

  logic.addConditions = function(){
    var data = {};
    data["direction"] = "x";
    data["operation"] = "=";
    data["coords"] = "0";
    data["mask"] = "+"
    logic.conditions[logic.global_count] = data;
    logic.global_count++; 

  }
  logic.removeConditions = function(key){
    delete logic.conditions[key];
  }
  logic.applyConditions = function(project_name){
    if(!logic.input_name){
      ServerCalls.messageError("Logic Name","Please enter the logic name");
      return;
    }
    ServerCalls.applyLogicConditions(logic,$scope,project_name,logic.input_name,logic.conditions);
  }
  
  // refactor making the below method as common
  logic.countLength=function(obj){
          var count = 0;
          for (var property in obj) {
              if (Object.prototype.hasOwnProperty.call(obj, property)) {
                  count++;
              }
          }

          return count;

        };

  logic.saveLocalData=function(litegraph_object){
      LabelGraphData["data"][logic.project_name]={}
      LabelGraphData["data"][logic.project_name][logic.network_name]=litegraph_object;
  }

  logic.saveDataToDB=function(){
      network_data=General.getNetworkInfo();
      logic_data=LabelGraphData["data"][logic.project_name][logic.network_name];
      network_data["logic_data"]=logic_data;
      ServerCalls.saveToDB(logic.project_name,network_data);
  }

  logic.getLabels=function(type='pore'){
    if(type == "throat")
     return GeoData[logic.project_name][logic.network_name]["throats"];
    else   
     return GeoData[logic.project_name][logic.network_name]["pores"];   
  }

  logic.getLogics=function(){
    return logic.logiclist;
  }

  

  logic.getLogicData=function(){
    console.log(logic.storage)
    return logic.storage;
  }

  logic.getStackList=function(){
    return logic.stack_storage;
  }

  logic.createPoreLabel=function(){
    var json_data={}
    logic.type="pores";
    json_data[logic.input_name]=$scope.getLabelData();
    var node = $scope.getNode();
    console.log(node.input_type);
    if(node.type == "pore_logic"){
      ServerCalls.applyPoreLabels($scope,logic.project_name,
      logic.input_name,json_data,node.input_type); 
    }else{
      ServerCalls.applyLogicLabels($scope,logic.project_name
      ,logic.type,logic.input_name,json_data); 
    }   
    //call to store in DB
    
    $("#modal_design").css("display","none");
    logic.input_name=null;
  }
  logic.createThroatLabel=function(){
    data=$scope.getLabelData()
    ServerCalls.applyThroatLabels($scope,logic.project_name,logic.input_name,data);
    $("#modal_design").css("display","none");
    logic.input_name=null;
  }
  

  logic.addLabel=function(name,type='pore'){
      if(type == 'throat'){
        $scope.addThroatLabel(name);
      }else{
        $scope.addPoresLabel(name);
      }
      
  }
  logic.addLogicNode=function(name){
      $scope.addLogicNode(name);
  }
  logic.addQueryNode=function(name){
      if(name.includes("pore")){
        $scope.addQueryNode(name,"pore_logic")
      }else{
        $scope.addQueryNode(name,"throat_logic")
      }
      
  }  
  logic.addThroatNode=function(name){
      $scope.addThroatNode(name)
  }  
  logic.removeNode=function(){
    $scope.removeNode();
  }
  logic.clearCanvas=function(){
    $scope.clear();
  }
  logic.resetZoom=function(){
    $scope.resetZoom();
  }
  logic.showLabelModal = function(){
    logic.label_modal = true;        
    $scope.showLabelModal();
  }
  logic.showModal=function(type){
    logic.type=type;
    if(type == "pores"){
      logic.type_flag=true;
    }else{
      logic.type_flag=false;
    }
    $scope.showModal();
  }



}]);
