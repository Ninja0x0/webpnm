angular.module("DashApp").controller("PhysicsController",['$scope','$http','ProjectsData','ServerCalls','General',function($scope,$http,ProjectsData,ServerCalls,General){
  var physics=this;
  physics.inputs={};
  physics.type={};
  physics.types={};
  physics.name=null;
  physics.copy_physics={};

  
  physics.init=function(){
    ServerCalls.getTypeData('Physics');

  }
  physics.clear=function(){
    physics.name=null;
  }

  physics.add=function(project_name,geometry_name,phase_name){
    network=General.getNetworkInfo();
    if(General.validation(physics.name)){
      value={name:physics.name,type:physics.type,models:{},
           phase:phase_name,geometry:geometry_name,
           inputs:physics.inputs,built_models:{},built_change:{}};    
      ProjectsData[project_name][network.name]["physics"][physics.name]=value;
      console.log(ProjectsData[project_name])
      ServerCalls.saveToDB(project_name,network);   
      physics.getBuiltInModels(network_name,value)
      physics.clear();
    }else{
      ServerCalls.messageError("Error","Name should not contain special characters and space.")
    }
    
  }

  physics.getTypes=function(){ 
    physics.types=ServerCalls.types['Physics'];   
    return ServerCalls.types['Physics'];   
  }

  physics.getBuiltInModels=function(network_name,physics_data){
    ServerCalls.getBuiltInModels('Physics',physics_data);
  }

  physics.getPresentPhysics=function(){
    return ResultData["data"][network_name]["physics"];
  }
  // // below code should be removed 
  // physics.makeDataFromPhysics=function(network_name,old_physics_name,new_physics_name){
  //    var results=ResultData["data"][network_name]["physics"][old_physics_name];
  //    return function(geometry_name,phase_name){
  //         var new_obj=Object.assign({},results);
  //         new_obj["name"]=new_physics_name;
  //         new_obj["geometry"]=geometry_name;
  //         new_obj["phase"]=phase_name;
  //         return new_obj;
  //    }
  // }

  // physics.copy=function(network_name,geometry_name,phase_name){
  //  var objects=physics.getPresentPhysics();
  //  var new_physics;
  //  while(true){
  //     new_physics=physics.makeID(physics.copy_physics);
  //     if (objects[new_physics] == undefined) {
  //       break;
  //     }
  //  }
  //   var generateData=physics.makeDataFromPhysics(network_name,physics.copy_physics,new_physics);
  //   var value=generateData(geometry_name,phase_name);
  //   ProjectsData["data"][network_name]["physics"][new_physics]=value;
  // }

  physics.makeID=function(physics_name){
    var text = "";
    var possible = "0123456789";
    for (var i = 0; i < 1; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return physics_name+text;
  }

}]);