angular.module("DashApp").controller("SimulationController",['$scope','ServerCalls','ProjectsData','General','NetworkData','AlgorithmsData','GeoData','GraphData',function($scope,ServerCalls,ProjectsData,General,NetworkData,AlgorithmsData,GeoData,GraphData){
    var simulation=this;
    simulation.built_modes={0:"add",1:"overwrite",2:"remove",3:"clear"}
    simulation.bc_types={0:"residual",1:"inlets",2:"outlets"}
    simulation.phases_list=""
    simulation.pores_list=""   
    simulation.network_name=""
    simulation.project_name=null;
    simulation.type=""
    simulation.sub_type=""
    simulation.inputs={}
    simulation.GRAPH=null;
    simulation.GRAPH_CANVAS=null;

    

    simulation.init=function(project_name){
      network=General.getNetworkInfo();
      ServerCalls.getMethods('Algorithms');      
      //ServerCalls.getAlgorithmsMethods(); //why need this
      simulation.network_name=network.name;
      simulation.project_name=project_name;
      simulation.phases_list=ProjectsData[project_name][simulation.network_name]["phases"];
      //console.log(GeoData)
      simulation.pores_list= GeoData[project_name][simulation.network_name]["pores"];         
    }

    simulation.saveLocalData=function(litegraph_object){
      console.log("litegraph",litegraph_object)
      GraphData["data"][simulation.project_name]={}
      GraphData["data"][simulation.project_name][simulation.network_name]=litegraph_object;
      
    }

    simulation.saveDataToDB=function(){
      network_data=General.getNetworkInfo();
      console.log(simulation)
      graph_data=GraphData["data"][simulation.project_name][simulation.network_name];
      network_data["graph_data"]=graph_data;
      ServerCalls.saveToDB(simulation.project_name,network_data);
    }

    simulation.getAlgorithms=function(){
      return AlgorithmsData["data"];
    }

    simulation.getAlgorithmsMethods=function(){       
      return AlgorithmsData["data"][simulation.type];
    }

    simulation.getMethodParamters=function(sub_type){
      return AlgorithmsData["data"][simulation.type][sub_type];
    }
    
    simulation.addAlgorithm=function(name){
      $scope.addAlgorithm(name)
    }
    simulation.nodeMenu=function(value){
      $scope.nodeMenu(value);
    }
    simulation.addPoresNode=function(name){
      $scope.addPoresNode(name);
    }
    simulation.addPhasesNode=function(name){
      $scope.addPhasesNode(name);
    }
    simulation.addMethodNode=function(name){
      $scope.addMethodNode(name);
    }
    simulation.addNode=function(name,type,inputs){
      $scope.addNode(name,type,inputs);
    }
    simulation.remove=function(){
      $scope.remove();
    }
    simulation.resetZoom=function(){
      $scope.resetZoom();
    }
    simulation.run=function(project_name){
      ProjectsData[project_name][simulation.network_name]["simulation"]=$scope.getAlgorithmData();
      configure_data=ProjectsData[project_name][simulation.network_name];
      ServerCalls.runAlgorithm(project_name,configure_data);
    }

}]);