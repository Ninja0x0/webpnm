// list of xhr calls 
angular.module("ServerCallsModule",[]).service("ServerCalls",function($http,Credentials,ProjectsData,NetworkData,ModelsData,GeoData,AlgorithmsData,General){    
 
    this.types={};
  
    this.modelsdata=ModelsData["data"];
    this.project_name=null;
    this.loadNetworkDB=function(network){
    cookie_name=network.getCookie("csrftoken");

    $http({
                method : "POST",
                url : "/api/load",
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
                
                if(response.data["message"]=="redirect")
                  window.location=response.data["path"]
                else{
                  Credentials=response.headers();
                  network.parseProject(response.data);               
                  network.spinner=false;
                }
                
                   

            }, function myError(response) {

               console.log("error",response);
            });
    }

    this.getTypeData=function(type){         
        cookie_name=this.getCookie("csrftoken");
        scope=this;
        $http({
                method : "POST",
                url : "/web/types/"+type,
                headers:{                  
                  "X-CSRFToken":cookie_name
                },                
            }).then(function mySuccess(response) {
                scope.types[type]=response.data;                                

            }, function myError(response) {
               console.log("error",response);
            });        
    }
    this.getCookie=function(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    this.getBuiltInModels=function(type,data){
      var url=[type.toLowerCase(),data.type].join("/")
      var network_data=General.getNetworkInfo();      
      var network_name=network_data.name;
      
      if(type=='Geometry'){           
           geometry_name=data["name"];                    
           network_data["geometries"][geometry_name]=data;
           built_models=network_data["geometries"][geometry_name];
      }
      else if(type=='Phases'){        
        phase_name=data["name"];       
        network_data["phases"][phase_name]=data;
        built_models=network_data["phases"][phase_name];      
      }
      else if(type=='Physics'){        
        physics_name=data["name"]; 
        network_data["physics"][physics_name]=data;
        built_models=network_data["physics"][physics_name];      
      }
      
      data={"network_name":network_name,"data":data};
      
      $http({
            method : "POST",
            url : "/api/attach/"+url,
            headers:{
              "Content-Type":"application/text",
              "X-CSRFToken":cookie_name
            },
            data:data
        }).then(function mySuccess(response) {
            console.log(response)
            built_models["built_models"]=Object.assign({},response.data);               
        }, function myError(response) {
           console.log("error",response);
        });
    }

    this.getModels=function(type,network_name,type_name){
     
      ModelsData["info"][type]=type_name;
      ModelsData["info"]["Network"]=network_name;
      ModelsData["type"]=type;
      cookie_name=this.getCookie("csrftoken");    
      $http({
                  method : "POST",
                  url : "/web/models/"+type+"/",
                  headers:{
                    "Content-Type":"application/json",
                    "X-CSRFToken":cookie_name
                  },
                  data:""
              }).then(function mySuccess(response) {

                  ModelsData["data"]=response.data;

                  

              }, function myError(response) {
                 console.log("error",response);
              });
    }

    

    this.fetchTypes=function(type,scope=null){    
    cookie_name=this.getCookie("csrftoken");

    $http({
                method : "POST",
                url : "/web/types/"+type,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:""
            }).then(function mySuccess(response) {
              type=type.toLowerCase();
              
              if(type == "network"){                
                NetworkData["data"]=response.data;
              }
              
                
              console.log(AlgorithmsData)
            }, function myError(response) {
               console.log("error",response);
            });


  }
  this.createNetwork=function(main,network,network_data=null){
    var server=this;
    cookie_name=this.getCookie("csrftoken");
    network_data=network.applyNetwork();    
    project_name=network.open_project;
    network.insertNetworkIntoProject(project_name,{[network_data.name]:network_data});  
    $http({
                method : "POST",
                url : "/api/project/"+network.open_project,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
            }).then(function mySuccess(response) {
                data=JSON.parse(JSON.stringify(response.data));
                network_name=data["network_name"]
                project_name=data["project_name"]
                GeoData[project_name]={}
                GeoData[project_name][network_name]={}
                GeoData[project_name][network_name]["attached_pores"]={}
                GeoData[project_name][network_name]["attached_throats"]={}               
                GeoData[project_name][network_name]["pores"]=data["pores"]
                GeoData[project_name][network_name]["throats"]=data["throats"]
                
                
                ProjectsData[project_name][network_name]["geo_data"]=GeoData[project_name][network_name]
                
                

                // network.insertNetworkIntoProject(project_name,{[network_name]:networks_data});
                network.hide_table=false;
                network.clearInputs();
                
                main.networks_data=General.getNetworkInfo();
                main.network_name=main.networks_data.name;

            }, function myError(response) {
               console.log("error",response);
            });
   }

  this.saveToDB=function(project_name,network_data){
    var server=this;
    cookie_name=server.getCookie("csrftoken");    
    var credentials={username:Credentials["username"],password:Credentials["password"]};
    

    $http({
                method : "POST",
                url : "/api/save/"+project_name,
                params:credentials,
                headers:{
                  "Content-Type":"application/json",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
            }).then(function mySuccess(response) {
               

                

            }, function myError(response) {
               console.log("error",response);
            });
  }

   this.getHelpString=function(module_name,class_name){
      cookie_name=this.getCookie("csrftoken");
      data={"module":module_name,"class":class_name}
      $http({
                method : "POST",
                url : "/api/help",
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                params:data
            }).then(function mySuccess(response) {

                $('#help-content').html(response.data);                

            }, function myError(response) {
               console.log("error",response);
            });
    }
    this.applyLabels=function(project_name){
      cookie_name=this.getCookie("csrftoken");
      data=LabelData["data"];
      $http({
                method : "POST",
                url : "/api/labels/"+network_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:data
            }).then(function mySuccess(response) {
                
                for(key in response.data){
                  console.log(key);
                  GeoData["data"][network_name]["pores"].push("pore."+key)
                  GeoData["data"][network_name]["pore_health"].push("pore."+key)
                  ResultData["data"][network_name]["labels"]["pore."+key]=response.data[key];
                  console.log(GeoData["data"][network_name]["pores"])
                }
                                


            }, function myError(response) {
               console.log("error",response);
            });
    }
    this.applyLogicLabels=function(scope,project_name,label_type,label_name,logic_data){
      network=General.getNetworkInfo();
      cookie_name=this.getCookie("csrftoken");
      logic_data["network_name"]=network.name;
      logic_data["label_type"]=label_type;
      logic_data["label_name"]=label_name;      
      $http({
                method : "POST",
                url : "/api/logiclabels/"+project_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:logic_data
            }).then(function mySuccess(response) {
                //create a node of label name attach the algorithim which created it
                var data=response.data
                GeoData[data.project_name][data.network_name][data.type].push(data.label_name)           
                scope.addAjaxPoreNode(data.label_name)                              

            }, function myError(response) {
               console.log("error",response);
            });
    }

    this.applyThroatLabels=function(scope,project_name,label_name,data){
      cookie_name=this.getCookie("csrftoken");
      network=General.getNetworkInfo();
      var json_data={}
      json_data[label_name]=data
      json_data["network_name"]=network.name;

      $http({
                method : "POST",
                url : "/api/throatlabels/"+project_name+"/"+label_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:json_data
            }).then(function mySuccess(response) {
                //create a node of label name attach the algorithim which created it
               var data=response.data
               GeoData[data.project_name][data.network_name][data.type].push(data.label_name)
               scope.addAjaxPoreNode(data.label_name)                 

            }, function myError(response) {
               console.log("error",response);
            });
    }


    
    this.checkGeometryHealthEx=function(geometry,network_name){
      cookie_name=this.getCookie("csrftoken");
      var server=this;
      data=General.getNetworkInfo();      
      $http({
                method : "POST",
                url : "/api/health/"+General.project_name+"/"+geometry.pores+"/"+geometry.throats,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:data
            }).then(function mySuccess(response) {
                data=response.data;

                if(!data["present"]){
                  $("#error-response").text("Pores Collision")
                  $(".error-message").show().delay(3000).fadeOut();                  
                }                     
                else{
                  name=General.project_name;                     
                  ProjectsData[name][network_name]["geometries"][geometry.name]=geometry.inputs_data;
                  geometry.getBuiltInModels();
                  geometry.attachPoresThroats(name);
                  console.log("sss",General.getNetworkInfo())
                  server.saveToDB(General.project_name,General.getNetworkInfo());

                  // geometry.checkPores(name,geometry.pores)
                  // geometry.checkThroats(name,geometry.throats)
                }                              


            }, function myError(response) {
               console.log("error",response);
            });
    }

    this.getMethods=function(type){
      cookie_name=this.getCookie("csrftoken");
      
      $http({
                method : "POST",
                url : "/web/methods/"+type,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                
            }).then(function mySuccess(response) {
                if(type.toLowerCase() == "algorithms"){
                  AlgorithmsData["data"]=response.data; 
                }                              


            }, function myError(response) {
               console.log("error",response);
            });
    }

    this.runAlgorithm=function(project_name,network_data){
     
      cookie_name=this.getCookie("csrftoken");
      
      $http({
                method : "POST",
                url : "/api/run/"+project_name,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
                
            }).then(function mySuccess(response) {
                var data=response["data"];
                var url=[project_name,data.hash].join("/");
                window.open(url);                                      


            }, function myError(response) {
               console.log("error",response);
            });
    }
    this.downloadFile=function(type,project_name){
      var server = this;
      network=General.getNetworkInfo();
      cookie_name=this.getCookie("csrftoken");
      var url=[type,project_name].join("/");
      $http({
                method : "POST",
                url : "/api/generate/"+url,
                headers:{
                  "Content-Type":"application/text",
                  "X-CSRFToken":cookie_name
                },
                data:network_data
                
            }).then(function mySuccess(response) {
                var data=response["data"];
                if(data.type == "hdf"){
                  var url=[window.host,"api","file",project_name,type,data.hash].join("/");
                  server.download(url)
                               
                }else if(data.type == "xmf"){
                  var url=[window.host,"api","file",project_name,"xmf",data.hash].join("/");
                  server.download(url)     
                }else{
                  var url=[window.host,"api","file",project_name,type,data.hash].join("/");
                  server.download(url)
                }
                
                  
                //window.location = url;
                                                      


            }, function myError(response) {
               console.log("error",response);
            });
    }

    this.remove=function(project_name){
      cookie_name=this.getCookie("csrftoken");
      $http({
            method : "POST",
            url : "/api/remove/"+project_name,
            headers:{
              "Content-Type":"application/text",
              "X-CSRFToken":cookie_name
            },
            
            
        }).then(function mySuccess(response) {
            var data=response["data"];
            window.location="/web/dashboard/";                                      


        }, function myError(response) {
           console.log("error",response);
        });
}

    this.downloadXMF=function(href){
      var anchor = document.createElement('a');
      anchor.href = href;
      anchor.download = href;
      document.body.appendChild(anchor);
      anchor.click();
    }

    this.download=function(href){
      var anchor = document.createElement('a');
      anchor.href = href;
      anchor.download = href;
      document.body.appendChild(anchor);
      anchor.click();
    }
    
    this.getNetworkList=function(){
      cookie_name=this.getCookie("csrftoken");
    }
    
}).service("General",function(ProjectsData){
  this.project_name=null;


  this.getNetworkInfo=function(){
    var general=this;
    var value=ProjectsData[general.project_name];   
    for(x in value){
     return value[x];
    }
    return;
  }
  
});

