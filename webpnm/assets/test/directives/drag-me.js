angular.module("DashApp").directive('dragMe', function(ProjectsData,General){
    return {
      restrict: 'A',
      scope: {
            physics_name: "@physicsName",
            project_name:"@projectName"          
      },
      link:function(scope,element,attr){        
        element.bind("dragstart",function(ev){
            network=General.getNetworkInfo();            
            var json_data=ProjectsData[scope.project_name][network.name]["physics"][scope.physics_name];           
            ev.dataTransfer.setData("text/plain",JSON.stringify(json_data));
            $('.physics_create').addClass('drop_here');
            
            
        });
        element.bind("dragend",function(ev){
            $('.physics_create').removeClass('drop_here');
            
        })
        element.bind("drag",function(ev){
         
        })
        
      }
     
    }
  });