angular.module("DashApp").directive('dropMe',function(General,ProjectsData,ServerCalls){
    return {
      restrict: 'A',
      scope:{
        project_name:"@projectName",
        phase_name:"@phaseName",
        geometry_name:"@geometryName"      
      },  
      link:function(scope,element,attr){

      var network=General.getNetworkInfo(); //fetch current open network

      var makeDataFromPhysics=function(project_name,old_physics_name,new_physics_name){
         var results=ProjectsData[scope.project_name][network.name]["physics"][old_physics_name];
         return function(geometry_name,phase_name){
              var new_obj=Object.assign({},results);
              new_obj["name"]=new_physics_name;
              new_obj["geometry"]=geometry_name;
              new_obj["phase"]=phase_name;
              return new_obj;
         }
      }

      var makeID=function(physics_name){
        var text = "";
        var possible = "0123456789";
        for (var i = 0; i < 1; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return physics_name+text;
      }
      var copy=function(project_name,geometry_name,phase_name,json_data){
       var objects=json_data;
       var new_physics;
       while(true){
          new_physics=makeID(json_data["name"]);
          if (objects[new_physics] == undefined) {
            break;
          }
       }
       
        var generateData=makeDataFromPhysics(network_name,json_data["name"],new_physics);
        var value=generateData(geometry_name,phase_name);
        ProjectsData[scope.project_name][network.name]["physics"][new_physics]=value;

      }

      



        element.bind("dragover",function(ev){       
          ev.preventDefault();
         
        })
        element.bind("drop",function(ev){        
          var json_data=JSON.parse(ev.dataTransfer.getData("text/plain"));
          console.log(json_data)
          ev.stopPropagation();
          ev.preventDefault();         
          copy(scope.project_name,scope.geometry_name,scope.phase_name,json_data)
          scope.$apply();
          ServerCalls.saveToDB(scope.project_name,network);
        });
      }
     
    }
  });