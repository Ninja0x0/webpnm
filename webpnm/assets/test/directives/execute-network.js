angular.module("DashApp").directive("executeNetwork",function(NetworkData){
  return{
    restrict:'EA',    
    link:function(scope,element,attrs){
      var camera, controls, scene, renderer;
      el=$("main")
      $("#canvas").attr("height",el[0].offsetHeight-(el[0].offsetHeight*5)/100);
      $("#canvas").attr("width",el[0].offsetWidth);
      
      var view_canvas=$("#canvas");
      var planex=$("#planex");
      var planey=$("#planey");
      var planez=$("#planez");
      var spherex=$("#spherex");
      var spherey=$("#spherey");
      var spherez=$("#spherez");
      var clone_width=$("#width");
      var clone_height=$("#height");
      var clone_depth=$("#depth");
      var sphere_radius=$("#radius");
   

      var label=scope.label;      
      var raycaster = new THREE.Raycaster();
      var mouse = new THREE.Vector2(),INTERSECTED;
      var objects=[];
      var CubeSize=checkString(label.getCube());      
      var LABEL_TYPE="Cube"
      var CURRENT_LABEL_NAME;
      var datastore={};


      var render = function(){
          controls.update();
          //coordinates(camera);
          renderer.render(scene,camera);
      }
      var animate=function(){
          requestAnimationFrame( animate );
          render();        
      }

      var init =function(){
          height=window.innerHeight;
          width=window.innerWidth;
          camera = new THREE.PerspectiveCamera( 45, width / height, 1, 1000 );
          camera.position.z=20; // should be
          camera.position.x=0;
          camera.position.y=0;
         
          renderer = new THREE.WebGLRenderer({ antialias: true });
          renderer.setSize( el[0].offsetWidth,el[0].offsetHeight-(el[0].offsetHeight*5)/100);
          renderer.setClearColor(0x131313);
          view_canvas.append( renderer.domElement );
          //var controls = new THREE.OrbitControls( camera, renderer.domElement );

          //Trackball controls
          controls = new THREE.TrackballControls( camera ,renderer.domElement);
          controls.rotateSpeed = 1.0;
          controls.zoomSpeed = 1.2;
          controls.panSpeed = 0.8;
          controls.noZoom = false;
          controls.noPan = false;
          controls.staticMoving = true;
          controls.dynamicDampingFactor = 0.3;

          scene = new THREE.Scene();
      }

      var initObjects=function(){

          MovableCubeSize=[CubeSize[0]/2,CubeSize[1]/2,CubeSize[2]/2];

          // geometries
          var geometry = new THREE.PlaneBufferGeometry( 1, 1);
          var main_geometry = new THREE.BoxGeometry( CubeSize[0], CubeSize[1], CubeSize[2] , 1, 1, 1);
          var sub_geometry = new THREE.BoxGeometry( MovableCubeSize[0], 
                                MovableCubeSize[1], 
                                MovableCubeSize[2] , 12, 12, 12); 
          var material = new THREE.MeshBasicMaterial( {color: 0x123436} );  
          //mesh     
          var cube_two = new THREE.Mesh( sub_geometry,material);
          var material = new THREE.MeshBasicMaterial( {color: 0xffffff,wireframe:true } );
          var cube_one = new THREE.Mesh( main_geometry,material);  

 
            
          
          


          
          
          cube_two.name="movable_cube";
          cube_two.position.x=(CubeSize[0]+(CubeSize[0]-MovableCubeSize[0]))/6; // shoudl be 4 but float changing
          cube_two.position.y=(CubeSize[1]+(CubeSize[1]-MovableCubeSize[1]))/6;
          cube_two.position.z=(CubeSize[2]+(CubeSize[2]-MovableCubeSize[2]))/6;

          planex.value=cube_two.position.x;
          planey.value=cube_two.position.y;
          planez.value=cube_two.position.z;
          planex.max=(CubeSize[0]+(CubeSize[0]-MovableCubeSize[0]))/6;
          planey.max=(CubeSize[1]+(CubeSize[1]-MovableCubeSize[1]))/6; 
          planez.max=(CubeSize[2]+(CubeSize[2]-MovableCubeSize[2]))/6;
          planex.min=-(CubeSize[0]+(CubeSize[0]-MovableCubeSize[0]))/6;
          planey.min=-(CubeSize[1]+(CubeSize[1]-MovableCubeSize[1]))/6;
          planez.min=-(CubeSize[2]+(CubeSize[2]-MovableCubeSize[2]))/6;

          //var new_geo=bsp.intersect(sbsp);
          //cube=new_geo.toMesh(material);
          clone_width.max=CubeSize[0]/2;  // 
          clone_height.max=CubeSize[1]/2;
          clone_depth.max=CubeSize[2]/2;
          clone_width.value=clone_width.max/3;
          clone_height.value=clone_height.max/3;
          clone_depth.value=clone_depth.max/3;
          
          
         
          // sphere mesh
          MAX_RADIUS=CubeSize.reduce(function(a,b){
              return Math.max(a,b);
          })
          RADIUS=MAX_RADIUS/4;
          var sphere_geometry = new THREE.SphereGeometry(RADIUS, 32, 32);
          var material = new THREE.MeshBasicMaterial( {color: 0x123436} );
          var sphere = new THREE.Mesh( sphere_geometry, material );
          sphere.name="movable_sphere";
          MAX_VALUE_X=(CubeSize[0]+(CubeSize[0]-MovableCubeSize[0]))/6; // shoudl be 4 but float changing
          MAX_VALUE_Y=(CubeSize[1]+(CubeSize[1]-MovableCubeSize[1]))/6;
          MAX_VALUE_Z=(CubeSize[2]+(CubeSize[2]-MovableCubeSize[2]))/6;
        
          spherex.min=-MAX_VALUE_X;
          spherey.min=-MAX_VALUE_Y;
          spherez.min=-MAX_VALUE_Z;
          spherex.max=MAX_VALUE_X;
          spherey.max=MAX_VALUE_Y;
          spherez.max=MAX_VALUE_Z;
          sphere_radius.value=sphere.radius;
          sphere_radius.max=MAX_RADIUS/2;
          
             
              
          scene.add(cube_one);
          scene.add( cube_two);
          objects.push(cube_one);
          objects.push(cube_two);
          objects.push(sphere);

      }
        var initListeners=function(){

          [cube_one,cube_two,sphere]=objects;

          planex.on('input',function(){ 
            cube_two.position.x=parseFloat(this.value);
                   
          });
          planey.on('input',function(){            
             cube_two.position.y=parseFloat(this.value);
            
          });
          planez.on('input',function(){            
             cube_two.position.z=parseFloat(this.value);       
             
          });
          
          spherex.on('input',function(){            
             sphere.position.x=parseFloat(this.value);                
             
          });

          spherey.on('input',function(){            
            sphere.position.y=parseFloat(this.value);         
             
          });

          spherez.on('input',function(){            
            sphere.position.z=parseFloat(this.value);              
            
          });

          sphere_radius.on('input',function(){
            value=parseFloat(this.value);         
            sphere.scale.set(value,value,value);        
            
          });

          clone_width.on('input',function(){
            cube_two.scale.x=parseFloat(this.value);           
           
          });
          clone_height.on('input',function(){ 
            cube_two.scale.y=parseFloat(this.value);          
            
          });
          clone_depth.on('input',function(){
            cube_two.scale.z=parseFloat(this.value);          
            
          });
          
      }
      var addLabel=function(){
          var material = new THREE.MeshBasicMaterial( {color: Math.random()*0xffffff,wireframe:true } );
          var selectedObject = scene.getObjectByName(label_name);
          scene.remove(selectedObject);
          if(LABEL_TYPE == "CUBE"){
          
            clone_cube=cube_two.clone();
            clone_cube.material=material;
            clone_cube.name=label_name;
            name=["pore",label_name].join(".");
            values=[clone_cube.position.x+(clone_width.value/2),
                clone_cube.position.y+(clone_height.value/2),
                clone_cube.position.z+(clone_depth.value/2)];
            console.log(label_name)
            datastore[label_name]={type:LABEL_TYPE,size:[clone_width.value,clone_height.value,clone_depth.value],
                         origin:[clone_cube.position.x,clone_cube.position.y,clone_cube.position.z],name:label_name.value,values:values,
                         geometry:clone_cube.geometry,material:clone_cube.material}
            
            label.update(datastore);
            //var obj=scene.getObjectByName('union_mesh');
            //scene.remove(obj);    
            //var main_network=new ThreeBSP(cube_one);
            //var sub_network=new ThreeBSP(cube_two);
            //var new_network=sub_network.intersect(main_network);
            //var material = new THREE.MeshBasicMaterial( {color: 0x343434} );
            //var newMesh=new_network.toMesh(material);
            //newMesh.name="union_mesh"
            
            scene.add(clone_cube);       
        
          }else{
            
            clone_sphere=sphere.clone();
            clone_sphere.material=material;
            clone_sphere.name=label_name;
            name=["pore",label_name].join(".");
            values=[clone_sphere.position.x+RADIUS,
                clone_sphere.position.y+RADIUS,
               clone_sphere.position.z+RADIUS];
            console.log(label_name)
            datastore[label_name.value]={type:LABEL_TYPE,size:[RADIUS],
                         origin:[sphere.position.x,sphere.position.y,sphere.position.z],name:label_name.value,values:values,geometry:clone_sphere.geometry,material:clone_sphere.material}
               
            label.update(datastore);         
            scene.add(clone_sphere);       
         
          }
          //animate();
      }

      label.renderLabel=function(label_name){
        var loader=label.getMesh(label_name);
        var selectedObject = scene.getObjectByName(CURRENT_LABEL_NAME);
        scene.remove(selectedObject);
        CURRENT_LABEL_NAME=label_name;
        var mesh_obj=new THREE.Mesh(loader.geometry,loader.material);
        mesh_obj.name=label_name;
        console.log("ss",scene);
        scene.add(mesh_obj);
        console.log(scene);
        render();

      }

      function checkString(value){
            value=value.replace("[",'');
            value=value.replace(']','');
            return value.split(',').map(function(item){
              return parseFloat(item);
            });
        
      }
      function deleteLabel(){
        delete datastore[label_name.value];
      }

      init();
      initObjects();
      initListeners();
      animate();
      // var label=scope.label;
      // scene = new THREE.Scene();
      // var raycaster = new THREE.Raycaster();
      // var mouse = new THREE.Vector2(),INTERSECTED;
      // var objects=[];
      // var CubeSize=checkString(label.getCube());
      
      // var LABEL_TYPE="Cube"
      // var CURRENT_LABEL_NAME;
      // var datastore={};

     
      // var label_name;
      // var planex=document.getElementById("planex");
      // var planey=document.getElementById("planey");
      // var planez=document.getElementById("planez");
      // var spherex=document.getElementById("spherex");
      // var spherey=document.getElementById("spherey");
      // var spherez=document.getElementById("spherez");
      // var clone_width=document.getElementById("width");
      // var clone_height=document.getElementById("height");
      // var clone_depth=document.getElementById("depth");
      // var sphere_radius=document.getElementById("radius");
   
      


      
      // var dragControls = new THREE.DragControls( objects, camera, renderer.domElement );
      // dragControls.addEventListener( 'dragstart', function ( event ) { controls.enabled = false; } );
      // dragControls.addEventListener( 'dragend', function ( event ) { controls.enabled = true; } );





      




      //trackball
      //controls.addEventListener( 'change', render); // add this only if there is no animation loop (requestAnimationFrame)

      
    
      
    


      //window.addEventListener( 'mousedown', onMouseDown, false );
      //window.addEventListener( 'mousemove', onMouseMove, false );
      
      // function onMouseDown( event ) {
        
      //   mouse.x = ( ( event.clientX - renderer.domElement.offsetLeft ) / renderer.domElement.width ) * 2 - 1;
      //   mouse.y = - ( ( event.clientY - renderer.domElement.offsetTop ) / renderer.domElement.height ) * 2 + 1;

      //   //camera.updateMatrixWorld();
      //         // find intersections

      //   raycaster.setFromCamera( mouse, camera );
      //   var intersects = raycaster.intersectObjects( scene.children );
      //   if ( intersects.length > 0 ) {
      //     if ( INTERSECTED != intersects[ 0 ].object ) {
      //       if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );
      //       INTERSECTED = intersects[ 0 ].object;
      //       console.log(INTERSECTED);
      //       INTERSECTED.material.color.setHex(0x393839);
      //       console.log("ass")
      //       if(INTERSECTED.type=="Mesh") {
      //         console.log("wwaw")
      //         information(INTERSECTED.name);
      //       }
            
             
      //     }
      //   } else {
          
      //     if ( INTERSECTED ){
      //                 INTERSECTED.material.color.setHex(0x393839);

      //     }
      //     INTERSECTED = null;
      //   }

      // }
      // function onMouseMove( event ) {
      //   mouse.x = ( ( event.clientX - renderer.domElement.offsetLeft ) / renderer.domElement.width ) * 2 - 1;
      //     mouse.y = - ( ( event.clientY - renderer.domElement.offsetTop ) / renderer.domElement.height ) * 2 + 1;

      //   //camera.updateMatrixWorld();
      //         // find intersections

      //   raycaster.setFromCamera( mouse, camera );
      //   var intersects = raycaster.intersectObjects( scene.children );
      //   if ( intersects.length > 0 ) {
      //     if ( INTERSECTED != intersects[ 0 ].object ) {
      //       if ( INTERSECTED ) INTERSECTED.material.color.setHex( INTERSECTED.currentHex );
      //       INTERSECTED = intersects[ 0 ].object;
            
      //       if(INTERSECTED.type=="Mesh") {
              
      //         $('html,body').css('cursor', 'pointer');
      //       }


      //     }
      //   } else {
      //     $('html,body').css('cursor', 'default');
      //     if ( INTERSECTED ){
      //                 INTERSECTED.material.color.setHex(0x393839);

      //     }
      //     INTERSECTED = null;
      //   }

      // }
      // function information(value){
      //   var el=document.querySelector(".objectname");
      //   var text=document.createTextNode(value);
      //   el.addEventListener('click',(event)=>{
      //       scene.remove(scene.getObjectByName(value));
      //       el.parentNode.removeChild(el);
      //     });
      //     el.appendChild(text);
      
      // }

      
      
    
     
    },

    
  }
})