angular.module("DashApp").directive('closeDropdown', function(){
    return {
      restrict: 'A',
      link:function(scope,element,attr){
        
        element.on('click',function(){            
            $("."+attr.closeDropdown).removeClass("show");  
       
       });       
      }     
    }
  });