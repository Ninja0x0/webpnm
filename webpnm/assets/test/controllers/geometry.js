describe('GeometryController', function() {
  beforeEach(module('DashApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('getTypes()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getInputs(geometry_type)', function() {
    it('return parameters of specific geometry type', function() {
      
    });
  });

  describe('getPores(project_name)', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('getThroats(project_name)', function() {
    it('return throat labels', function() {
      
    });
  });


});
