describe('NetworkController', function() {
  beforeEach(module('DashApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('validateProject()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getProjectList()', function() {
    it('return parameters of specific geometry type', function() {
      
    });
  });

  describe('getNetworkInfo()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getNetworkData()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('getNetworkInputs()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('applyNetwork()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('viewNetwork()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('viewTypes()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('viewInputTypes()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('viewGeometryDivision()', function() {
    it('return pores labels', function() {
      
    });
  });


});

