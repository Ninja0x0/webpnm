describe('LabelLogicController', function() {
  beforeEach(module('DashApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('getLabels()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getLogics()', function() {
    it('return parameters of specific geometry type', function() {
      
    });
  });

  describe('getLogicData()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getStackList()', function() {
    it('return pores labels', function() {
      
    });
  });

});

