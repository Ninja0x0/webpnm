describe('ModelController', function() {
  beforeEach(module('DashApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('getInputData()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getBuiltModels()', function() {
    it('return parameters of specific geometry type', function() {
      
    });
  });

  describe('getAttachedModels()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getModelList()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('getModelNames()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('getModelMethods()', function() {
    it('return pores labels', function() {
      
    });
  });

  describe('getModelInputs()', function() {
    it('return pores labels', function() {
      
    });
  });

});

