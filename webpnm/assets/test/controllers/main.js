describe('MainController', function() {
  beforeEach(module('DashApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('getGeometry()', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getPhases()', function() {
    it('return parameters of specific geometry type', function() {
      
    });
  });

  describe('getPhysicsBoth(geometry_name,phase_name)', function() {
    it('return ajax response containing types of geometry', function() {
      
    });
  });

  describe('getPhysicsData(geometry_name,phase_name)', function() {
    it('return pores labels', function() {
      
    });
  });

});

