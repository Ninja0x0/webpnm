from setuptools import setup

setup(
    name='webpnm',
    description = '.....',    
    install_requires=[
        'openpnm==2.0.0b2',
        "requests",
        'pymongo',
        'django'
    ],
    packages=[
    	"webpnm",
    	"webpnm/db",
    	"webpnm/assets",
    	"webpnm/Storage",
    	"webpnm/webpnm",
    	"webpnm/public",
    	"webpnm/api",
    	"webpnm/client",    	
    ],
    entry_points={  # Optional
        'console_scripts': [
            'webpnm=webpnm.command_line:main',
        ],
    },
    include_package_data=True,
    author='OpenPNM Team',
    author_email='jeff.gostick@waterloo',
)